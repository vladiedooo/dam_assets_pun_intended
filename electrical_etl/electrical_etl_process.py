# coding=utf-8

"""This module, electrical_etl_process.py, launches the entire Electrical ETL Process."""

# Used for colorful console printing.
from lazyme.string import color_print
# Used to get current directory.
import os
# Used for measuring time.
from universal_code import anything_time_related as atr
# Used for parsing the DAM.
from electrical_etl.electrical_dam_parser import ElectricalDAMParser
# Used for parsing Excel files.
from electrical_etl.electrical_csv_parser import ElectricalExcelParser
# Used to represent unique SKUs as products.
from electrical_etl.electrical_classes import ElectricalProduct
# Used to run ETL error checks and polish data.
from electrical_etl.electrical_error_checks import ElectricalErrorChecker
# Used to get a glance at tree configuration options.
from electrical_etl.sku_tree_distribution_tester import TreeTester
# Used to make RESTfull calls to AEM.
from electrical_etl.aem_rest_api import AEMCalls
# Used for running pre-defined procedures with Docker containers.
from electrical_etl.docker_api import DockerManager
from electrical_etl.docker_api import DockerContainer
# Used to construct the final CSV.
from electrical_etl.electrical_csv_parser import AEMCSVAssets


# Useful variable to keep track of here
DOCKER_DAM_PATH = '/aem/crx-quickstart/bin/DAM/electrical/DAM'
CSV_PATH        = '/aem/crx-quickstart/bin/DAM/electrical/'


class ElectricalETLProcess:
	"""Runs the Electrical ETL process."""

	def __init__(self):
		self.current_path        = os.path.dirname(os.path.realpath(__file__))
		self.excel_path          = self.current_path + '/electrical_files/electrical_images.xlsx'
		self.csv_to_send         = self.current_path + '/electrical_files/final_csv.csv'
		self.sample_csv_to_send  = self.csv_to_send.replace('final_csv', 'sample_final_csv')
		self.dam_path            = self.current_path + '/electrical_files/DAM'
		self.dam_to_send_path    = self.current_path + '/electrical_files/DAM_TO_SEND/'
		self.excel_parser        = ElectricalExcelParser(self.excel_path)
		self.dam_parser          = ElectricalDAMParser(self.dam_path)
		self.products            = []
		self.etl_error_checks    = None
		self.tree_tester         = None
		self.aem_api             = AEMCalls()
		self.docker_api          = DockerManager()
		self.author_container    = self.docker_api.get_docker_instance('author')
		self.final_csv_generator = None

	def _get_clean_products(self):
		clean_products = []
		for p in self.products:
			if p.clean:
				clean_products.append(p)
		return clean_products

	def _get_clean_assets(self):
		clean_assets = []
		for a in self.dam_parser.assets:
			if a.clean:
				clean_assets.append(a)
		return clean_assets

	def run_etl_process(self):
		"""This runs the entire ETL process.
		:return: Void."""
		color_print('Starting the ETL process :', color='blue', bold=True, underline=True)
		timer = atr.EasyTimer()

		# ETL Step 0x0.
		color_print('Reading the Excel file into memory, then closing it...', underline=True, end='')
		self.products = self.excel_parser.parse_out_products(sheet_names_to_ignore='all images')
		color_print(timer.get_incremental_time(), bold=True)
		color_print('Currently have ' + str(len(self.products)) + ' unique SKUs.\n', color='yellow')

		# ETL Step 0x1.
		color_print('Reading the DAM asset information into memory...', underline=True, end='')
		information = self.dam_parser.parse_asset_information_and_return_report_as_string()
		color_print(timer.get_incremental_time(), bold=True)
		color_print(information + '\n', color='yellow')

		# ETL Step 0x2.
		color_print('Running the ETL error checks...', underline=True, end='')
		self.etl_error_checks = ElectricalErrorChecker(self.products, self.dam_parser.assets)
		clean, dirty, products_fixed, assets_fixed = self.etl_error_checks.run_error_checks_and_polish_product_objects_then_return_information_output()
		color_print(timer.get_incremental_time(), bold=True)
		color_print('There are ', color='yellow', end='')
		color_print(str(clean), color='blue', bold=True, end='')
		color_print(' clean products and ', color='yellow', end='')
		color_print(str(dirty), color='red', bold=True, end='')
		color_print(' non-clean products. ', color='yellow', end='')
		color_print(str(products_fixed), color='blue', bold=True, end='')
		color_print(' products were fixed and ', color='yellow', end='')
		color_print(str(assets_fixed), color='blue', bold=True, end='')
		color_print(' assets were fixed.\n', color='yellow')

		print('')
		color_print('Printing potential solutions :', color='yellow')
		self.etl_error_checks.print_solutions()

		# ETL Step 0x3.
		color_print('Printing tree configuration options :', underline=True)
		self.tree_tester = TreeTester(self.products)
		self.tree_tester.print_tree_configurations()
		color_print(timer.get_incremental_time(), bold=True)

		# ETL Step 0x4. (Currently skipping because this step takes hours)
		print('')
		color_print('Compressing the asset files into a new DAM folder to send to AEM...', underline=True, end='')
		#self.dam_parser.create_dam_to_send_to_aem(self.products, self.dam_to_send_path)
		color_print(timer.get_incremental_time(), bold=True)

		# ETL Step 0x5.
		print('')
		color_print('Constructing the CSV file to upload to local Docker/AEM...', underline=True, end='')
		self.final_csv_generator = AEMCSVAssets(self.products, self.dam_parser.assets, self.csv_to_send)
		color_print(timer.get_incremental_time(), bold=True)

		# ETL Step 0x6.
		print('')
		color_print('Uploading files to local AEM instance...', underline=True, end='')
		self.author_container.copy_local_files_to_this_instance(self.sample_csv_to_send, DockerContainer.CSV_PATH + 'sample_csv.csv')
		#self.author_container.copy_local_files_to_this_instance(self.csv_to_send, DockerContainer.CSV_PATH + 'final_csv.csv')
		self.author_container.copy_local_files_to_this_instance(self.dam_path, DockerContainer.DOCKER_DAM_PATH)
		color_print(timer.get_incremental_time(), bold=True)

		# TODO : Disable workflows here.

		# ETL Step 0x7.
		print('')
		color_print('Testing file upload.', underline=True)
		# TODO : Only run this automatically once running it manually works.
		#self.aem_api.launch_csv_asset_upload()

electrical_etl = ElectricalETLProcess()
electrical_etl.run_etl_process()




