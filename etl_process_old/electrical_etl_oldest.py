# coding=utf-8

"""
This module, excel_file_parser.py, will read Excel files.
"""

# Used to read Excel files.
import openpyxl as o
# Used for getting absolute paths.
import os
# Used for zipfile functionality.
import zipfile
# Used for tarfile functionality.
import tarfile
# Used for executing regular expressions.
import re
# Used for recursively getting files from a directory.
import glob

# The file to load. TODO : Change to dynamic solution once more concrete business requirements are established.
FILE_PATH              = 'electrical_images.xlsx'
DAM_ZIP_PATH           = 'DAM.zip'
CURRENT_PATH           = os.path.dirname(os.path.abspath(__file__))
TEMP_OUTPUT            = CURRENT_PATH + '/dam_temp_output'

# Utility variables.
ALL_IMAGES_TAB_NAME    = 'all images'
PRIMARY_IMAGE_NAME     = 'Primary Image Name'
USE_GENERIC_IMAGE      = 'use generic image'
DISCONTINUED           = 'discontinued?'
SHOULD_THESE_BE_LISTED = 'should these be listed?'
NONE                   = 'none'

ERROR_WORDS            = [DISCONTINUED, SHOULD_THESE_BE_LISTED, USE_GENERIC_IMAGE, NONE]

# Regular expression to match AEM's rules for Classic UI.
AEM_TEXT_REGULAR_EXPRESSION = re.compile('[\w\-]+')


'''  __                 __   ___           ___  ___  __      __   __   __   ___
	|  \  /\   |\/|    |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	|__/ /~~\  |  |    |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''


# TODO : USE THIS!!
# TODO : USE THE RECOMMENDED AEM MAPPING FOR INVALID CHARACTERS.
def is_text_aem_compliant(text: str) -> bool:
	"""Checks if the provided text abides by AEM's naming guidance rules.
	:param text: The text to check against.
	:return: Boolean indicating if the text is compliant or not.
	"""
	is_valid = True
	for c in text:
		if c != '_' and c != '-' and not c.isalnum() and not c.isalpha():
			is_valid = False
			break
	return is_valid

'''
print(is_text_aem_compliant('app@le'))
print(is_text_aem_compliant('apple2'))
print(is_text_aem_compliant('apple '))
print(is_text_aem_compliant('-_apple'))
exit()
'''


class DAM:
	"""A representation of the DAM folder provided.
	"""

	def __init__(self, path_to_dam, clean_products):
		self.clean_products = clean_products
		self.all_files      = {}

		# TODO : Add a ton of error checking to this step.

		print('Extracting contents from the DAM zip!')
		# TODO : Add this back in, it's gone for now just to speed up local deployment.
		'''
		if zipfile.is_zipfile(path_to_dam):
			zip_file = zipfile.ZipFile(path_to_dam, 'r')
			zip_file.extractall(TEMP_OUTPUT)
			zip_file.close()
		'''

		for filename in glob.glob(TEMP_OUTPUT + '/DAM/**', recursive=True):
			if not os.path.isdir(filename):
				full_path = filename.replace(TEMP_OUTPUT + '/DAM', '')
				if not full_path.endswith('.db'):
					self.all_files[full_path[full_path.rfind('/') + 1:]] = full_path[0:full_path.rfind('/')]

		self.all_files[USE_GENERIC_IMAGE] = 'TODO : Find out where to get the generic image from.'

	def analyze(self):
		"""Run steps relating to the DAM specifically.
		:return:
		"""
		print('Running DAM analysis! Matching products to their respective files.')
		bad_products = []
		for p in self.clean_products:
			primary_image = p.properties_dictionary[PRIMARY_IMAGE_NAME]
			if primary_image.lower().strip() != USE_GENERIC_IMAGE:
				if primary_image not in self.all_files:
					# Bad data check 4. Making sure this asset has a file to reference.
					bad_products.append(p)
				else:
					p.file_path = self.all_files[primary_image] + primary_image

		# Remove the bad products.
		for p in bad_products:
			self.clean_products.remove(p)

'''  __   __   __   __        __  ___     __   ___           ___  ___  __      __   __   __   ___
	|__) |__) /  \ |  \ |  | /  `  |     |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	|    |  \ \__/ |__/ \__/ \__,  |     |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''


class ProductsAnalyzer:
	"""Pre-defined functions designed to work with a list of products.
	"""

	def __init__(self):
		self.sku_to_product_objects = {}
		self.products               = {}
		self.bad_skus               = []
		self.clean_products         = []

	def get_clean_products(self):
		"""Now finally inspected the gathered data.
		:return: Void.
		"""
		print('Doing first round of parsing out bad products!')
		for sku in self.products:
			combined_dictionary = {}
			for property_dictionary in self.products[sku]:
				for key in property_dictionary:
					if key not in combined_dictionary:
						combined_dictionary[key] = property_dictionary[key]
					else:
						# Bad data check 0.
						# TODO : Investigate more into this later.
						if str(combined_dictionary[key]) != str(property_dictionary[key]):
							self.bad_skus.append(sku)

			# Bad data check 1. Checking if the SKU even has a primary image attribute.
			if PRIMARY_IMAGE_NAME not in combined_dictionary:
				self.bad_skus.append(sku)
			else:
				for key in combined_dictionary:
					if key == PRIMARY_IMAGE_NAME:
						da_value = str(combined_dictionary[key]).lower().strip()
						# Bad data check 2. Making sure the primary image is not an invalid primary image value.
						if da_value in ERROR_WORDS:
							self.bad_skus.append(sku)
						# Bad data check 3. Make sure the primary image value is not just the string ''.
						elif da_value == '':
							self.bad_skus.append(sku)

			if sku not in self.bad_skus:
				self.clean_products.append(CleanProduct(sku, combined_dictionary))

		print('There are ' + str(len(self.products)) + ' unique SKUs.')
		print('There are ' + str(len(self.clean_products)) + ' products, (pre-error checks).')

		return self.clean_products

	def add_products(self, sku_to_products_dictionary):
		"""Add products to keep analyze later.
		:param sku_to_products_dictionary: A dictionary of key-sku to value-products_dictionary.
		:return: Void.
		"""
		for sku in sku_to_products_dictionary:
			if sku not in self.sku_to_product_objects:
				self.sku_to_product_objects[sku] = sku_to_products_dictionary[sku]

			if sku not in self.products:
				self.products[sku] = []
			for property_dictionary in sku_to_products_dictionary[sku].list_of_property_dictionaries:
				self.products[sku].append(property_dictionary)


# TODO : Clean up the product classes.


class CleanProduct:
	"""Each object instance maps to a unique SKU product with fully gathered and cleaned information.
	"""

	def __init__(self, sku, properties_dictionary):
		self.sku                   = sku
		self.properties_dictionary = properties_dictionary
		self.file_path             = None

	def __str__(self):
		return str(self.sku) + ' : ' + str(self.properties_dictionary) + ' : ' + str(self.file_path)


class Product:
	"""Each object instance maps to a unique SKU product.
	"""

	def __init__(self, sku, business_unit):
		self.sku                           = sku
		self.business_unit                 = business_unit
		self.list_of_property_dictionaries = []

	def add_property_dictionary(self, property_dictionary):
		"""Add a property dictionary to this product.
		:param property_dictionary: The product dictionary to add.
		:return: Void.
		"""
		self.list_of_property_dictionaries.append(property_dictionary)

	def get_properties_and_fail_if_there_is_more_than_one(self):
		"""Utility function to get properties from a product that only has 1 set of properties.
		:return: The properties dictionary.
		"""
		if len(self.list_of_property_dictionaries) != 1:
			print('Error! This length of list of properties is : ' + str(len(self.list_of_property_dictionaries)))
			exit()
		else:
			return self.list_of_property_dictionaries[0]

	def __str__(self):
		pretty_text = ''
		for property_dictionary in self.list_of_property_dictionaries:
			pretty_text += str(property_dictionary) + '\n'
		return 'sku{' + str(self.sku) + '} ' + pretty_text

'''  ___      __   ___          __   ___           ___  ___  __      __   __   __   ___
	|__  \_/ /  ` |__  |       |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	|___ / \ \__, |___ |___    |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''


class WorksheetGeneric:
	"""Parent class to WorksheetBusiness and WorksheetAllImages. Defines all generic worksheet operations.
	"""

	def __init__(self, sheet_name, worksheet):
		"""
		:param sheet_name: The name of the worksheet (located on the tab).
		:param worksheet: An openpyxl worksheet object.
		"""
		self.sheet_name       = sheet_name
		self.worksheet        = worksheet
		self.headers          = []
		self.data_rows        = []
		self.products         = {}
		for i, row in enumerate(self.worksheet.rows):
			if i == 0:
				for cell in row:
					self.headers.append(str(cell.value))
			else:
				data = []
				for cell in row:
					data.append(str(cell.value))
				self.data_rows.append(data)

	def _add_product(self, product):
		"""Utility function to add a product. If a product SKU appears more than once then the existing product SKU will gain those additional properties.
		:param product: The product object to add or extract data from.
		:return: Void.
		"""
		sku = product.sku
		if sku not in self.products:
			self.products[sku] = product
		else:
			self.products[sku].add_property_dictionary(product.get_properties_and_fail_if_there_is_more_than_one())

	def parse(self):
		"""Grab data from the sheet.
		:return: Void.
		"""
		raise Exception("Function {parse} not defined in {" + str(self.__class__.__name__) + "} ¯\_(ツ)_/¯")


class WorksheetBusiness(WorksheetGeneric):
	"""The standard tab type in the worksheet.
	"""

	def __init__(self, sheet_name, worksheet):
		super().__init__(sheet_name, worksheet)

	def parse(self):
		"""Grab data from the sheet.
		:return: Void.
		"""
		# Each row is a product.
		for row in self.data_rows:
			product             = Product(str(row[0]).strip(), self.sheet_name)
			property_dictionary = {}
			for i, cell in enumerate(row[1:]):
				if cell != 'None':
					property_dictionary[self.headers[i + 1]] = cell
			product.add_property_dictionary(property_dictionary)
			self._add_product(product)


class WorksheetAllImages(WorksheetGeneric):
	"""Unique tab in the in worksheet.
	"""

	def __init__(self, sheet_name, worksheet):
		super().__init__(sheet_name, worksheet)

	def parse(self):
		"""Grab data from the sheet.
		:return: Void.
		"""
		# NOTE : This worksheet is not used for now.
		y = 2


class ElectricalImagesFileParser:
	"""This object parses the electrical images file.
	"""

	def __init__(self, file_name):
		self.file_name         = file_name
		self.file_path         = os.path.abspath(FILE_PATH)
		self.workbook          = None
		self.worksheets        = []
		self.products_analyzer = ProductsAnalyzer()
		self.dam               = None

	def _add_worksheet(self, worksheet: WorksheetGeneric):
		"""Utility function to add a worksheet.
		:param worksheet: The GenericWorksheet object to add (will either be a WorksheetBusiness or an WorksheetAllImages
		:return: Void.
		"""
		self.worksheets.append(worksheet)

	def parse(self):
		"""Read the file information into program memory.
		:return: Void.
		"""
		self.workbook   = o.load_workbook(self.file_path, read_only=True)
		all_sheet_names = self.workbook.get_sheet_names()
		for sheet_name in all_sheet_names:
			if sheet_name == ALL_IMAGES_TAB_NAME:
				self.worksheets.append(WorksheetAllImages(sheet_name, self.workbook[sheet_name]))
			else:
				self.worksheets.append(WorksheetBusiness(sheet_name, self.workbook[sheet_name]))

		for worksheet in self.worksheets:
			worksheet.parse()
			self.products_analyzer.add_products(worksheet.products)

		self.dam = DAM(DAM_ZIP_PATH, self.products_analyzer.get_clean_products())
		self.dam.analyze()

		clean_products = self.dam.clean_products

		print('There are ' + str(len(clean_products)) + ' clean products!')
		print('Printing the clean products!')
		for p in clean_products:
			print(p)

parser = ElectricalImagesFileParser(FILE_PATH)
parser.parse()
