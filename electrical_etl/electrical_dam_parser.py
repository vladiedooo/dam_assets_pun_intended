# coding=utf-8

"""This module, electrical_dam_parser.py, will unzip and parse the contents provided from DAM.zip."""

# TODO : Add zip features in later.
# Used for zipfile functionality.
import zipfile
# Used for tarfile functionality.
import tarfile
# Used for recursively getting files from a directory.
import glob
from glob2 import iglob
# Used to represent electrical asset files.
from electrical_etl.electrical_classes import ElectricalAsset
# Used to represent electrical products.
from electrical_etl.electrical_classes import ElectricalProduct
# Used for OS operations.
import os
# To convert values to human readable formats.
import humanize
# Used to help the IDE.
from typing import List
# Used for running bash commands.
from electrical_etl import bash_command_runner as bcr
# Used for running compression.
from PIL import Image


class ElectricalDAMParser:
	"""Parses the Electrical DAM.zip"""

	def __init__(self, directory_path):
		self.directory_path       = directory_path
		self.assets               = []
		self._original_size       = str(humanize.naturalsize(sum(os.path.getsize(x) for x in iglob(self.directory_path + '/**'))))
		self._original_raw_size   = str(sum(os.path.getsize(x) for x in iglob(self.directory_path + '/**')))
		self._compressed_size     = str(humanize.naturalsize(sum(os.path.getsize(x) for x in iglob(self.directory_path.replace('/electrical_files/DAM', '/electrical_files/DAM_TO_SEND') + '/**'))))
		self._compressed_raw_size = str(sum(os.path.getsize(x) for x in iglob(self.directory_path.replace('/electrical_files/DAM', '/electrical_files/DAM_TO_SEND') + '/**')))

	def get_original_size_easy_to_read(self):
		"""Returns the size of the provided DAM converted to the most logical unit (made up example : 562346123 bytes -> 4.5 GB)
		:return: A string representation of the original size of the DAM."""
		return self._original_size

	def get_original_size_raw_bytes(self):
		"""Returns the size of the provided DAM as the raw bytes size.
		:return: A string representation of the original size of the DAM."""
		return self._original_raw_size

	def parse_asset_information_and_return_report_as_string(self):
		"""Get information about the assets files.
		:return: Void."""
		for filename in glob.glob(self.directory_path + '/**', recursive=True):
			# Ignore directories, only look at files.
			if not os.path.isdir(filename):
				self.assets.append(ElectricalAsset(filename))
				# We don't need files that end in '.db'.
				if self.assets[-1].file_ending == 'db':
					self.assets = self.assets[:-1]
		return 'Found ' + str(len(self.assets)) + ' asset files. Size is ' + self.get_original_size_easy_to_read() + ' or to be exact : ' + self.get_original_size_raw_bytes() + ' bytes. Here is the compressed file size : {' + str(self._compressed_size) + ' or ' + str(self._compressed_raw_size) + '}'

	def create_dam_to_send_to_aem(self, products: List[ElectricalProduct], dam_to_send_path):
		"""Copies files from the DAM to the new DAM, will also compress JPEG files to 85% quality (the web standard).
		:return: Void."""
		for p in products:
			for k in p.properties:
				if k != 'sku' and k != 'original image name' and k != 'product name' and str(p.properties[k]).lower().strip() != 'use generic image':

					search_value = str(p.properties[k])
					asset = None
					for a in self.assets:
						if search_value == a.file_name:
							asset = a
						if a.final_name is not None:
							if search_value == a.final_name:
								asset = a

					# TODO : Investigate why this is happening.
					if asset is not None:
						send_directory = dam_to_send_path + asset.relative_directory_path
						send_path      = send_directory + asset.get_name()
						# First make the directory.
						bcr.run_bash_command_and_get_output(['mkdir', '-p', send_directory])
						if asset.file_ending == 'jpg':

							try:
								print(asset.file_path)
								foo = Image.open(asset.file_path)
								foo.save(send_path, quality=90)
							except Exception as e:
								print('Error trying to compress a file!')

							# TODO : In the future run this with the optimize=True flag

							#print(send_path)
							#print(bcr.run_bash_command_in_the_container_and_get_output(['jpegoptim', 'd', '"' + send_directory + '"', 'm', '90', 'f', '"' + asset.file_path + '"']))
						else:

							bcr.run_bash_command_and_get_output(['cp', asset.file_path, send_path])
							#print(asset)
							#print(send_path)

