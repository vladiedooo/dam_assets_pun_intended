# coding=utf-8

"""This module, send_files_to_docker.py, is just a small utility script to send DAM asset files to the local Docker instance."""

# Needed for executing bash commands.
from electrical_etl import bash_command_runner as bcr


class DockerContainer:
	"""A one to one representation of running Docker containers."""

	DOCKER_DAM_PATH     = '/aem/crx-quickstart/bin/DAM/electrical/DAM'
	CSV_PATH            = '/aem/crx-quickstart/bin/DAM/electrical/'

	def __init__(self, row):
		self.container_id      = str(row[0])
		self.container_image   = str(row[1])
		self.container_command = str(row[2])
		self.container_name    = str(row[-1])

	def run_bash_command_in_the_container_and_get_output(self, bash_command):
		"""Runs the provided back command on the docker container.
		:param bash_command: The bash command to run and see the output of.
		:return: The output as a string."""
		if type(bash_command) == list:
			print('Don\'t pass a list, just a string!')
			exit()
		result = bcr.run_bash_command_and_get_output(['docker', 'exec', '-t', self.container_id, 'bash', '-c', bash_command])
		return result

	def copy_local_files_to_this_instance(self, local_path, destination_path):
		"""Copies the provided directory into the docker container.
		:param local_path       : The local path to the data to be transferred.
		:param destination_path : The destination path for within the docker instance.
		:return: Void."""
		self.run_bash_command_in_the_container_and_get_output('mkdir DAM/electrical/')

		# Was used for debugging :
		'''
		print(local_path)
		print(type(local_path))
		print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')

		print(self.container_id)
		print(type(self.container_id))
		print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')

		print(destination_path)
		print(type(destination_path))
		print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
		print('---------------------------------\n')
		'''

		# TODO : check if the previous error went away (cant convert to string explicitly).

		command = 'docker cp \'' + local_path + '\' ' + self.container_id + ':' + destination_path
		bcr.run_bash_command_and_get_output(command, cwd='/Users/utarsuno/git_repos/dam_assets_project_git_repos/ideal-aem-magento-stack', shell=True)

		# Now verify that they are there.

	def __str__(self):
		return self.container_id + '\t' + self.container_image + '\t' + self.container_command + '\t' + self.container_name


class DockerManager:
	"""Helps retrieve and populate DockerContainer objects."""

	DOCKER                        = 'docker'
	GET_RUNNING_DOCKER_CONTAINERS = [DOCKER, 'ps']

	def __init__(self):
		self._login_to_ecs_docker_repo()
		self.docker_containers  = {}
		self._get_currently_running_docker_containers()

	def _login_to_ecs_docker_repo(self):
		try:
			log_in_command = bcr.run_bash_command_and_get_output(['aws', 'ecr', 'get-login', '--region', 'us-east-1']).split()
			result         = bcr.run_bash_command_and_get_output(log_in_command)
			if result == 'Login Succeeded':
				self._logged_in = True
		except Exception as e:
			print('Error logging in : {' + str(e) + '}')
			exit()

	def _get_currently_running_docker_containers(self):
		result = bcr.run_bash_command_and_get_output(DockerManager.GET_RUNNING_DOCKER_CONTAINERS).split('\n')
		for i, row in enumerate(result):
			split = row.split()
			if i != 0:
				if len(split) > 0:
					docker_instance = DockerContainer(row.split())
					self.docker_containers[docker_instance.container_name] = docker_instance

	def get_docker_instance(self, name_substring_match) -> DockerContainer:
		"""Returns a DockerContainer object of the searched for (by name substring match) docker instance.
		:param name_substring_match: The partial text to match the name with.
		:return: The DockerContainer object or None if not found."""
		for dc in self.docker_containers:
			if name_substring_match in dc:
				return self.docker_containers[dc]


