# coding=utf-8

"""This module, electrical_etl.py, runs the etl process for electrical. Once the next project arrives it will become much easier to generalize this process."""

# Used for debugging.
from universal_code import debugging as dbg
# Used for time measuring.
from universal_code import anything_time_related as atr

'''  __        __   __                __   ___           ___  ___  __      __   __   __   ___
	/ _` |    /  \ |__)  /\  |       |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	\__> |___ \__/ |__) /~~\ |___    |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''






'''  ___      __   ___          __   ___           ___  ___  __      __   __   __   ___
	|__  \_/ /  ` |__  |       |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	|___ / \ \__, |___ |___    |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''


class WorksheetGeneric:
	"""Defines generic worksheet operations."""

	def __init__(self, sheet_name, worksheet):
		"""
		:param sheet_name: The name of the worksheet (located on the tab in the Excel file).
		:param worksheet:  An openpyxl worksheet object.
		"""
		self.sheet_name       = sheet_name
		self.worksheet        = worksheet
		self.headers          = []
		self.data_rows        = []

	def parse(self):
		"""Grab data from the sheet.
		:return: Void."""
		for i, row in enumerate(self.worksheet.rows):
			if i == 0:
				for cell in row:
					self.headers.append(str(cell.value).strip().lower())
			else:
				data = []
				for cell in row:
					data.append(str(cell.value).strip())
				self.data_rows.append(data)


class WorksheetElectrical(WorksheetGeneric):
	"""Standard tab provided in the electrical worksheet."""

	def __init__(self, sheet_name, worksheet):
		super().__init__(sheet_name, worksheet)

'''  ___ ___          __   __   __   __   ___  __   __      __   ___           ___  ___  __      __   __   __   ___
	|__   |  |       |__) |__) /  \ /  ` |__  /__` /__`    |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	|___  |  |___    |    |  \ \__/ \__, |___ .__/ .__/    |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''

