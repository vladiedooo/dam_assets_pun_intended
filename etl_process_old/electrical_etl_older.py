# coding=utf-8

"""This module, electrical_etl.py, runs the etl process for electrical. Once the next project arrives it will become much easier to generalize this process.
"""

# Used to read Excel files.
import openpyxl as o
# Used for getting absolute paths.
import os
# Used for zipfile functionality.
import zipfile
# Used for tarfile functionality.
import tarfile
# Used for executing regular expressions.
import re
# Used for recursively getting files from a directory.
import glob
from glob2 import iglob
# Used for mediocre time measurements.
from universal_code import anything_time_related as atr
# Used for colorful console printing.
from lazyme.string import color_print
# Used for finding closes match to a String from a list of Strings.
import difflib
# Does simple lexical analysis.
import shlex
# Needed for launching sub-processes.
import subprocess
# Needed for doing system operations.
import sys
# Needed for doing valid Image check operations.
from PIL import Image
# Needed for getting MIME types.
from mimetypes import MimeTypes
# Needed for copying files.
import shutil
# To convert values to human readable formats.
import humanize
# Needed for getting the standard deviation.
import statistics

'''  __        __   __                __   ___           ___  ___  __      __   __   __   ___
	/ _` |    /  \ |__)  /\  |       |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	\__> |___ \__/ |__) /~~\ |___    |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''

# Utility variables (for IDE auto-complete).
ALL_IMAGES_TAB_NAME         = 'all images'
PRODUCT_NAME                = 'product name'
ORIGINAL_IMAGE_NAME         = 'original image name'
PRIMARY_IMAGE_NAME          = 'primary image name'
SECOND_IMAGE                = 'secondary image'
TERTIARY_IMAGE              = 'tertiary image'
QUATERNARY_IMAGE            = 'quaternary image'
QUINARY_IMAGE               = 'quinary image'
PDF_SELL_SHEET              = 'pdf-sell sheet'
PDF_SPEC_SHEET              = 'pdf-spec sheet'
PDF_SPEC_SHEET_2            = 'pdf-spec sheet 2'
PDF_INSTRUCTIONS            = 'pdf-instructions'
PDF_WARRANTY                = 'pdf-warranty'
PDF_MISCELLANEOUS           = 'pdf-miscellaneous'
USE_GENERIC_IMAGE           = 'use generic image'
USE_GENERIC_PATH            = 'use generic path'
DISCONTINUED                = 'discontinued?'
SHOULD_THESE_BE_LISTED      = 'should these be listed?'
NONE                        = 'none'


# Important variables.
FILE_NAME                   = 'electrical_images.xlsx'
DAM_ZIP_NAME                = 'DAM.zip'
CURRENT_PATH                = os.path.dirname(os.path.abspath(__file__))
TEMP_OUTPUT                 = CURRENT_PATH + '/dam_temp_output'
COMPRESSION_OUTPUT          = CURRENT_PATH + '/dam_compression_output'
FAULTY_PRIMARY_IMAGE_VALUES = [DISCONTINUED, SHOULD_THESE_BE_LISTED, USE_GENERIC_IMAGE, NONE]
IMAGE_EXTENSIONS            = {'jpg': 'image/jpeg', 'psd': 'image/vnd.adobe.photoshop', 'eps': 'application/octet-stream', 'pdf': 'application/pdf'}


# Used for getting mime types.
mime_type_getter = MimeTypes()
# TODO : Generate regular expressions.


def run_bash_command_and_get_output(bash_command, shell=False, cwd=None):
	"""Runs the provided bash command and returns the output.
	:param bash_command : The bash command to run, as a list of String parameters.
	:param shell        : TODO - document this.
	:param cwd          : The current working directory that this command should be ran from.
	:return: The output of the bash command, as a string."""
	bad_argument = False
	if type(bash_command) != list and shell is False:
		bad_argument = True
	else:
		if shell is False:
			for c in bash_command:
				if type(c) != str:
					bad_argument = True
	if bad_argument:
		print('Bad argument! {' + str(bash_command) + '}')
		exit()
	# Safety checks are over.
	if cwd is not None:
		result = subprocess.run(bash_command, stdout=subprocess.PIPE, shell=shell, cwd=cwd)
	else:
		result = subprocess.run(bash_command, stdout=subprocess.PIPE, shell=shell)
	return result.stdout.decode('utf-8')


class AutoFixes:
	"""Each instance describes an instance where an automatic fix was taken such as adding the proper file extension type."""

	def __init__(self):
		self.fixes = []

	def add_fix(self, description):
		"""Add a fix to print out later.
		:param description: The text to print.
		:return: Void."""
		self.fixes.append(description)

	def print_all_fixes(self):
		"""This will print all the fixes that the program made.
		:return: Void."""
		print('\n')
		color_print('Printing all potential fixes that can be made :', color='blue', underline=True)
		for f in self.fixes:
			print(f)

# This will be the only global class to reference. Since we want this output to be displayed after the entire ETL process has finished.
auto_fixes = AutoFixes()


def terminate_due_to_programmer_mistake(debug_message):
	"""Just a utility program to catch any small logic mistakes.
	:param debug_message: The message to print out.
	:return: Void.
	"""
	color_print(str(debug_message), color='red', bold=True)
	exit(1)

'''  __                 __   ___           ___  ___  __      __   __   __   ___
	|  \  /\   |\/|    |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	|__/ /~~\  |  |    |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''


class AssetFile:
	"""A representation of an Asset file.
	"""

	def __init__(self, file_path):
		if file_path == USE_GENERIC_IMAGE:
			self.file_path  = USE_GENERIC_PATH
			self.file_name  = USE_GENERIC_IMAGE
			self.is_generic = True
		elif file_path == USE_GENERIC_PATH:
			self.file_path  = USE_GENERIC_PATH
			self.file_name  = USE_GENERIC_IMAGE
			self.is_generic = True
		else:
			self.is_generic = False
			self.file_path  = TEMP_OUTPUT + '/DAM' + file_path
			self.file_name  = self.file_path[self.file_path.rfind('/') + 1:]
		self.mime_type = None

	def parse(self):
		"""Utility function to gather information about this Asset file.
		:return: Void."""
		if not self.is_generic:
			self.mime_type = self._get_mime_type()

	def compress(self):
		"""Utility function to compress this file.
		:return: Void."""
		y = 2

	def _get_mime_type(self):
		cmd       = shlex.split('file --mime-type {0}'.format('"' + self.file_path + '"'))
		result    = subprocess.check_output(cmd)
		mime_type = result.split()[-1]
		return mime_type.decode('ascii')

	def __str__(self):
		if hasattr(self, 'file_name'):
			return self.file_name + '\tmime_type{' + str(self.mime_type) + '}'
		else:
			return 'Generic Asset'


class DAM:
	"""A representation of the DAM (zip)folder provided."""

	def __init__(self, path_to_dam, perform_unzip):
		"""
		:param path_to_dam   : The location of the zip file.
		:param perform_unzip : If locally used pre-zipped data should be used.
		"""
		self.path_to_dam   = path_to_dam
		self.perform_unzip = perform_unzip
		self.assets        = {USE_GENERIC_IMAGE: AssetFile(USE_GENERIC_PATH)}
		if self.perform_unzip:
			if zipfile.is_zipfile(path_to_dam):
				zip_file = zipfile.ZipFile(path_to_dam, 'r')
				zip_file.extractall(TEMP_OUTPUT)
				zip_file.close()

		for filename in glob.glob(TEMP_OUTPUT + '/DAM/**', recursive=True):
			if not os.path.isdir(filename):
				full_path = filename.replace(TEMP_OUTPUT + '/DAM', '')
				if not full_path.endswith('.db'):
					self.assets[full_path[full_path.rfind('/') + 1:]] = AssetFile(full_path)

	def process_assets(self):
		"""Utility function to process the Asset files to gather information and check for various errors.
		:return: Void."""
		for asset in self.assets:
			self.assets[asset].parse()

	def compress_assets(self, products, actually_compress):
		"""Utility function to compress the assets.
		:param products          : List of products to get file paths from.
		:param actually_compress : The compression may already have been ran so is not needed to be run again.
		:return: Void."""
		if actually_compress:
			for p in products:
				product = products[p]
				if product.clean:

					for key in product.properties:
						if key.lower() != PRODUCT_NAME:
							value = product.properties[key]
							if value != USE_GENERIC_IMAGE:
								source_path      = str(self.assets[product.properties[key]].file_path)
								destination_path = source_path.replace('dam_temp_output/DAM', 'dam_compression_output/DAM')
								destination_path = destination_path[:destination_path.rfind('/')]
								#shutil.copyfile(source_path, destination_path)
								#shutil.copy2(source_path, destination_path)
								#print(str(key) + ' : ' + str(product.properties[key]) + '\t' + str(self.assets[product.properties[key]].file_path))
								#copyfile(str(self.assets[product.properties[key]].file_path), str(self.assets[product.properties[key]].file_path).replace('dam_temp_output', 'dam_compression_output'))

	def print_compression_ratio(self):
		"""Simply print the compression that was achieved.
		:return: Void."""
		color_print('Note : The after file will also be smaller because only clean SKUs get transferred.', bold=True)
		print('Before : ' + str(humanize.naturalsize(sum(os.path.getsize(x) for x in iglob(TEMP_OUTPUT + '/DAM/**')))))
		print('After : ' + str(humanize.naturalsize(sum(os.path.getsize(x) for x in iglob(COMPRESSION_OUTPUT + '/DAM/**')))))
		print('Exact values : {' + str(sum(os.path.getsize(x) for x in iglob(TEMP_OUTPUT + '/DAM/**'))) + ' / ' + str(sum(os.path.getsize(x) for x in iglob(COMPRESSION_OUTPUT + '/DAM/**'))) + '}')


class TreeOptimizationTesting:
	"""This object will test the various ways that the tree can be constructed to find the most efficient configuration."""

	def __init__(self, sku_list):
		self.sku_list = sku_list

	def _test_level_(self, level):
		"""Test a specific tree level.
		:param level: The level to test.
		:return: Void."""
		sku_dictionary = {}
		for sku in self.sku_list:
			if sku[:level] not in sku_dictionary:
				sku_dictionary[sku[:level]] = ['node']
			else:
				sku_dictionary[sku[:level]].append('node')

		# TODO : Make sure that no level of child nodes has > 1000 or even close to that. Don't just use the average number.
		number_of_child_nodes = 0
		nums                  = []
		for sku in sku_dictionary:
			number_of_child_nodes += len(sku_dictionary[sku])
			nums.append(len(sku_dictionary[sku]))
		average_number_of_child_nodes = float(number_of_child_nodes) / float(len(sku_dictionary))

		return len(sku_dictionary), average_number_of_child_nodes, statistics.stdev(nums)

	def test_tree_configurations(self):
		"""Run analysis to find the preferred tree configuration.
		:return: Void."""
		longest_sku  = -1
		smallest_sku = 99999
		for sku in self.sku_list:
			sku_length = len(sku)
			if sku_length > longest_sku:
				longest_sku = sku_length
			if sku_length < smallest_sku:
				smallest_sku = sku_length

		index = 1
		while index != smallest_sku + 1:
			a, b, c = self._test_level_(index)
			print('Putting SKUs into sub folders based of the first ' + str(index) + ' characters gives out : ' + str(a) + ' folders with an average of ' + str(b) + ' nodes per folder. With a standard deviation of ' + str(c) + '. For a total of around ' + str(int(float(a) * float(b))) + ' total files + folders.')
			index += 1
'''  __   __   __   __        __  ___     __   ___           ___  ___  __      __   __   __   ___
	|__) |__) /  \ |  \ |  | /  `  |     |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	|    |  \ \__/ |__/ \__/ \__,  |     |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''

_ERROR_SKU_EQUALS_THE_TEXT_NONE            = 'Error : The SKU text equals \'None\'!'                                                        # 0x0.
_ERROR_PRIMARY_IMAGE_KEY_DOES_NOT_EXIST    = 'Error : Primary image key does not exist!'                                                    # 0x1.
_ERROR_PRIMARY_IMAGE_VALUE_IS_INVALID      = 'Error : Primary image value is invalid!'                                                      # 0x2.
_ERROR_DUPLICATE_SKU_WITH_DIFFERENT_VALUES = 'Error : There is a duplicate SKU but the properties do not match the current existing one!'   # 0x3.
_ERROR_NO_MATCHING_ASSET_FILE              = 'Error : No matching asset file!'                                                              # 0x4.
_ERROR_MISMATCHED_MIME_TYPES               = 'Error : Mis-matched mime types!'                                                              # 0x5.
_ERROR_ASSET_FILE_ERRORS                   = 'Error : The asset file is corrupt!'                                                           # 0x6.
_ERROR_NO_DOT_EXTENSION_ON_FILE            = 'Error : No dot extension on the file!'                                                        # 0x7.
_ERROR_CHECK_FOR_MISC_FILE_ERRORS          = 'Error : The file has misc file errors!'                                                       # 0x8.
_ERROR_CHECK_OTHER_VALUE_TO_FILE_MATCHES   = 'Error : A property does not match up against a file!'                                         # 0x9.

ERROR_TYPES = (_ERROR_SKU_EQUALS_THE_TEXT_NONE,
               _ERROR_PRIMARY_IMAGE_KEY_DOES_NOT_EXIST,
               _ERROR_PRIMARY_IMAGE_VALUE_IS_INVALID,
               _ERROR_DUPLICATE_SKU_WITH_DIFFERENT_VALUES,
               _ERROR_NO_MATCHING_ASSET_FILE,
			   _ERROR_MISMATCHED_MIME_TYPES,
               _ERROR_ASSET_FILE_ERRORS,
               _ERROR_NO_DOT_EXTENSION_ON_FILE,
               _ERROR_CHECK_FOR_MISC_FILE_ERRORS,
               _ERROR_CHECK_OTHER_VALUE_TO_FILE_MATCHES)


class ProductError:
	"""Describes the specific error that the Product has.
	"""

	def __init__(self, error_description, error_type, additional_info=None):
		self.error_description = error_description
		self.error_type        = error_type
		self.additional_info   = additional_info
		if self.error_type not in ERROR_TYPES:
			terminate_due_to_programmer_mistake('Not a valid error type!')

	def __str__(self):
		if self.error_type == _ERROR_PRIMARY_IMAGE_KEY_DOES_NOT_EXIST:
			return str(self.additional_info)
		return self.error_description


class Product:
	"""Each object instance maps to a unique SKU.
	Note : This object can be in the following states (default of 'Not Clean':
			- Clean     : Ready to be sent to AEM Servlet for further processing.
			- Not Clean : Will not be sent to AEM, error reports available.
	"""

	def __init__(self, sku):
		self.sku                           = sku
		self.product_objects_with_same_sku = []
		self.properties                    = {}
		self.currently_passing_errors      = True
		self.clean                         = False
		self.error                         = None
		self.ending                        = None
		self.primary_image_name            = None
		self.is_generic                    = False
		self.assets                        = []

	def add_asset(self, asset):
		self.assets.append(asset)

	def set_properties(self, properties):
		"""Set the properties for this product.
		:param properties: The properties to set of off.
		:return: Void."""
		self.properties         = properties
		if PRIMARY_IMAGE_NAME in self.properties:
			self.primary_image_name = self.properties[PRIMARY_IMAGE_NAME]
			if self.primary_image_name.lower() == USE_GENERIC_IMAGE:
				self.is_generic = True
			elif '.' in self.primary_image_name:
				self.ending = self.primary_image_name[self.primary_image_name.rfind('.') + 1:]

	def run_all_error_checks(self, dam):
		"""This function will run the error checks.
		:param dam : A DAM object.
		:return: Void."""

		# TODO : EACH PROPERTY SHOULD HAVE ITS OWN VERSIONS OF CERTAIN ERROR CHECKS, THAT INCLUDES HISTORY OF PAST RAN ERROR CHECKS.

		self._error_check_0_valid_sku()
		self._error_check_1_duplicate_sku_with_different_values()
		self._error_check_2_check_for_an_existing_primary_image_key()
		self._error_check_3_check_for_valid_primary_image_value()

		for key in self.properties:
			if key != PRODUCT_NAME:
				self._error_check_4_check_for_matching_asset_file(dam, self.properties[key])
				self._error_check_5_check_for_dot_extension_on_asset_files(dam, self.properties[key])

		self._error_check_6_check_for_matching_mime_types(dam)
		self._error_check_7_check_for_misc_file_errors(dam)

		self.clean = self.currently_passing_errors

	def _add_error(self, error_message, error_type, additional_info=None):
		self.error                    = ProductError(error_message, error_type, additional_info)
		self.currently_passing_errors = False

	def _error_check_0_valid_sku(self):
		if self.sku == 'None':
			self._add_error('There exists an Excel row with SKU of {' + str(self.sku) + '}', _ERROR_SKU_EQUALS_THE_TEXT_NONE)

	def _error_check_1_duplicate_sku_with_different_values(self):
		if self.currently_passing_errors:
			if len(self.product_objects_with_same_sku) > 0:
				self._add_error('Investigate this SKU (the error is easier explained in person): ' + str(self.sku), _ERROR_DUPLICATE_SKU_WITH_DIFFERENT_VALUES)

	def _error_check_2_check_for_an_existing_primary_image_key(self):
		if self.currently_passing_errors:
			if PRIMARY_IMAGE_NAME not in self.properties:
				self._add_error('The ' + PRIMARY_IMAGE_NAME + ' key property does not exist.', _ERROR_PRIMARY_IMAGE_KEY_DOES_NOT_EXIST, self)

	def _error_check_3_check_for_valid_primary_image_value(self):
		if self.currently_passing_errors:
			if not self.is_generic:
				primary_image_name = self.primary_image_name.lower()
				if primary_image_name in FAULTY_PRIMARY_IMAGE_VALUES or primary_image_name == '':
					self._add_error('The value for the key{' + PRIMARY_IMAGE_NAME + '} is invalid. It has a value{' + self.primary_image_name + '}.', _ERROR_PRIMARY_IMAGE_VALUE_IS_INVALID)

	def _error_check_4_check_for_matching_asset_file(self, dam, file_name):
		if self.currently_passing_errors:
			if file_name.lower() != USE_GENERIC_IMAGE:
				if file_name not in dam.assets and not self.is_generic:
					#close_matches = difflib.get_close_matches(self.primary_image_name, dam.assets.keys())
					self._add_error('For SKU{' + str(self.sku) + '} the following asset file was not found {' + file_name + '}!', _ERROR_NO_MATCHING_ASSET_FILE, 'not reporting close matches for now')

					# Just adding a potential fix to report.
					if self.primary_image_name.endswith('jpg'):
						if self.primary_image_name[:-3] + '.jpg' in dam.assets:
							auto_fixes.add_fix('For SKU{' + str(self.sku) + '} the asset file was found after fixing this faulty image name {' + self.primary_image_name + '}, by adding a period before the \'jpg\'!')

	def _error_check_5_check_for_dot_extension_on_asset_files(self, dam, file_name):
		if self.currently_passing_errors:
			if file_name.lower() != USE_GENERIC_IMAGE:
				if '.' in file_name:
					ending = file_name[file_name.rfind('.') + 1:]
					if ending not in IMAGE_EXTENSIONS:
						self._add_error('The ending{' + str(ending) + '} was not found to be a valid file extension. From the file of ' + str(file_name) + '}', _ERROR_NO_DOT_EXTENSION_ON_FILE)
						#print(str(dam.assets[file_name].mime_type))
				else:
					if file_name in dam.assets:
						auto_fixes.add_fix('For SKU{' + str(self.sku) + '} the issue file {' + str(file_name) + '} could be potentially fixed by appending the file ending for {' + str(dam.assets[file_name].mime_type) + '}')
					self._add_error('The ending{} was not found to be a valid file extension. From the file of {' + str(file_name) + '}', _ERROR_NO_DOT_EXTENSION_ON_FILE)

	def _error_check_6_check_for_matching_mime_types(self, dam):
		if self.currently_passing_errors:
			if not dam.assets[self.primary_image_name].is_generic:
				if self.ending is not None:
					if IMAGE_EXTENSIONS[self.ending] != dam.assets[self.primary_image_name].mime_type:
						self._add_error('File name is{' + str(self.primary_image_name) + '}, file inspection on asset file got {' + str(dam.assets[self.primary_image_name].mime_type) + '}', _ERROR_MISMATCHED_MIME_TYPES, [self.primary_image_name, dam.assets[self.primary_image_name].mime_type])
				else:
					self._add_error('Weird. For the following product [' + str(dam.assets[self.primary_image_name]) + '] did not having a \'self.ending\'?. It was None.', _ERROR_NO_DOT_EXTENSION_ON_FILE)

	def _error_check_7_check_for_misc_file_errors(self, dam):
		if self.currently_passing_errors:
			if not dam.assets[self.primary_image_name].is_generic:
				if self.ending != 'psd':
					try:
						image = Image.open(dam.assets[self.primary_image_name].file_path)
						image.verify()
					except Exception as e:
						self._add_error('For SKU{' + str(self.sku) + '} with file name{' + str(self.primary_image_name) + '} there was the file exception{' + str(e) + '}!', _ERROR_CHECK_FOR_MISC_FILE_ERRORS)

	def add_product_that_has_same_sku(self, product):
		"""Only the parent product objects will have their product_objects_with_same_sku list populated.
		:param product: The product object instance to keep track of.
		:return: Void."""
		if self.properties != product.properties:
			self.product_objects_with_same_sku.append(product)

	def __str__(self):
		return self.sku + ' properties:' + str(self.properties)

'''  ___      __   ___          __   ___           ___  ___  __      __   __   __   ___
	|__  \_/ /  ` |__  |       |__) |__  |     /\   |  |__  |  \    /  ` /  \ |  \ |__
	|___ / \ \__, |___ |___    |  \ |___ |___ /~~\  |  |___ |__/    \__, \__/ |__/ |___
'''


class WorksheetGeneric:
	"""Parent class to WorksheetBusiness and WorksheetAllImages. Defines all generic worksheet operations.
	Note : The 'All Images' tab is not to be referenced. So WorksheetAllImages class will not be defined in this file.
	"""

	def __init__(self, sheet_name, worksheet, etl_process):
		"""
		:param sheet_name: The name of the worksheet (located on the tab in the Excel file).
		:param worksheet:  An openpyxl worksheet object.
		"""
		self.sheet_name       = sheet_name
		self.worksheet        = worksheet
		self.etl_process      = etl_process
		self.headers          = []
		self.data_rows        = []
		for i, row in enumerate(self.worksheet.rows):
			if i == 0:
				for cell in row:
					self.headers.append(str(cell.value).strip().lower())
			else:
				data = []
				for cell in row:
					data.append(str(cell.value).strip())
				self.data_rows.append(data)

	def parse(self):
		"""Grab data from the sheet.
		:return: Void.
		"""
		raise Exception("Function {parse} not defined in {" + str(self.__class__.__name__) + "} ¯\_(ツ)_/¯")


class WorksheetElectrical(WorksheetGeneric):
	"""The standard tab type in the electrical worksheet.
	"""

	def __init__(self, sheet_name, worksheet, etl_process):
		super().__init__(sheet_name, worksheet, etl_process)

	def parse(self):
		"""Grab data from the sheet.
		:return: Void.
		"""
		# Each row is a product.
		for row in self.data_rows:
			sku = str(row[0]).strip()
			if sku != 'None':
				product    = Product(sku)
				properties = {}
				for i, cell in enumerate(row[1:]):
					if cell != 'None':
						if str(cell).strip().lower() == USE_GENERIC_IMAGE:
							properties[self.headers[i + 1]] = USE_GENERIC_IMAGE
						else:
							properties[self.headers[i + 1]] = str(cell).strip()

				if ORIGINAL_IMAGE_NAME in properties:
					del properties[ORIGINAL_IMAGE_NAME]

				product.set_properties(properties)
				self.etl_process.add_product(product)


class ElectricalETLProcess:
	"""The Electrical ETL process.
	"""

	def __init__(self, file_name):
		self.file_name         = file_name
		self.file_path         = os.path.abspath(self.file_name)
		self.workbook          = None
		self.worksheets        = []
		self.products          = {}
		self.dam               = None
		self.errors            = {}
		self.tree_configurer   = None

	def add_product(self, product):
		"""Utility function to add a product object.
		:param product: The product object to add.
		:return: Void.
		"""
		sku = product.sku
		if sku not in self.products:
			self.products[sku] = product
		else:
			self.products[sku].add_product_that_has_same_sku(product)

	def parse(self):
		"""Read the file information into program memory.
		:return:
		"""
		color_print('Starting the ETL process :', color='yellow', bold=True, underline=True)
		timer = atr.EasyTimer()

		print('Reading the Excel data...', end='')
		self.workbook = o.load_workbook(self.file_path, read_only=True)
		for sheet_name in self.workbook.get_sheet_names():
			if sheet_name != ALL_IMAGES_TAB_NAME:
				self.worksheets.append(WorksheetElectrical(sheet_name, self.workbook[sheet_name], self))

		for worksheet in self.worksheets:
			worksheet.parse()
		self.workbook.close()
		color_print(timer.get_incremental_time(), bold=True)

		print('Unzipping the DAM zip file...', end='')
		dam = DAM(DAM_ZIP_NAME, False)
		color_print(timer.get_incremental_time(), bold=True)

		print('Processing the Asset files...', end='')
		dam.process_assets()
		color_print(timer.get_incremental_time(), bold=True)

		print('Running the error checks...', end='')
		clean_products = 0
		dirty_products = 0
		for p in self.products:
			product = self.products[p]
			product.run_all_error_checks(dam)
			if product.clean:
				clean_products += 1
			else:
				dirty_products += 1
		color_print(timer.get_incremental_time(), bold=True)

		print('Running the compression on asset files...', end='')
		dam.compress_assets(self.products, True)
		color_print(timer.get_incremental_time(), bold=True)

		print('There are currently ' + str(len(self.products)) + ' unique SKUs/products. The clean/dirty ratio --> {', end='')
		color_print(str(clean_products), color='blue', bold=True, end='')
		print('/', end='')
		color_print(str(dirty_products), color='red', bold=True, end='')
		print('}')
		print('')

		color_print('Printing compression achieved :', color='yellow', underline=True)
		dam.print_compression_ratio()
		print('')

		#color_print('Printing utility information for optimization :', color='yellow', underline=True)
		#self._run_tree_optimizer()

		#print('')
		#color_print('Printing the list of product issues :', color='yellow', underline=True)
		#self._print_all_product_issues()
		#auto_fixes.print_all_fixes()


		number_of_products_printed = 0
		products_to_generate = []
		print('')
		color_print('Printing all the products!', color='blue', underline=True)
		for p in self.products:
			#print(self.products[p])
			if len(self.products[p].properties) == 2 and self.products[p].properties[PRIMARY_IMAGE_NAME] != USE_GENERIC_IMAGE:
				number_of_products_printed += 1
				if number_of_products_printed < 10:
					products_to_generate.append(self.products[p])
					#print(self.products[p])

		print('\n\n')
		self.generate_csv_data(products_to_generate)

	def generate_csv_data(self, products):
		"""This function will generate the CSV data to send to AEM.
		:param products: The products to include into this CSV data.
		:return: Void."""
		# TODO : Move these variables later.
		csv = ''

		columns = []
		columns.append('absTargetPath')
		columns.append('relSrcPath')
		columns.append('dam:productID {{ String }}')
		columns.append('dc:title {{ String }}')
		columns.append('imageName {{ String }}')
		columns.append('../metadata/SKU {{ String }}')
		columns.append('../metadata/primary_image {{ String }}')
		columns.append('../metadata/secondary_image {{ String }}')
		columns.append('../metadata/tertiary_image {{ String }}')
		columns.append('../metadata/quaternary_image {{ String }}')
		columns.append('../metadata/quinary_image {{ String }}')
		columns.append('../metadata/pdf_sell_sheet {{ String }}')
		columns.append('../metadata/pdf_spec_sheet {{ String }}')
		columns.append('../metadata/pdf_spec_sheet_2 {{ String }}')
		columns.append('../metadata/pdf_instructions {{ String }}')
		columns.append('../metadata/pdf_warranty {{ String }}')
		columns.append('../metadata/pdf_miscellaneous {{ String }}')

		local_dam_path      = '/Users/utarsuno/git_repos/scc/my_scc_projects/dam_assets_pun_intended/etl_process/dam_temp_output/DAM'
		local_dam_zip_path  = '/Users/utarsuno/git_repos/scc/my_scc_projects/dam_assets_pun_intended/etl_process/DAM.zip'
		docker_dam_zip_path = '/aem/crx-quickstart/bin/DAM/electrical/DAM.zip'
		docker_dam_path     = '/aem/crx-quickstart/bin/DAM/electrical/DAM'

		dam_path_start      = '/content/dam/electrical/'
		sku_node_path_start = '/etc/commerce/products/electrical/electrical/en/buckets/'

		for c in columns:
			csv += str(c) + ','
		csv += '\n'

		for p in products:
			csv += dam_path_start + ''
			print(p)

	def _run_tree_optimizer(self):
		"""Utility function to run the tree optimizer.
		:return: Void."""
		skus = []
		for p in self.products:
			if self.products[p].clean:
				skus.append(p)
		self.tree_configurer = TreeOptimizationTesting(skus)
		self.tree_configurer.test_tree_configurations()

	def _print_all_product_issues(self):
		"""Utility function to print all the current product issues.
		:return: Void."""
		print('Printing out the issues with the dirty products.')

		for error_type in ERROR_TYPES:
			error_type_printed = False
			for p in self.products:
				product = self.products[p]
				if product.error is not None:
					if product.error.error_type == error_type:

						if error_type_printed is False:
							color_print(error_type, color='red', underline=True)
							error_type_printed = True

						print(product.error)

etl = ElectricalETLProcess(FILE_NAME)
etl.parse()




'''
Putting in the compressor code here : add the compression code in later.


# -*- coding: utf-8 -*-

"""
This module, image_compressor, defines a system to take in a zipped file, un-zip it, compress the files inside, then zip the files back up.

last generated : 2017-04-14 10:05:20

Base File Layout:
------------------------------------------------------------------------------------------
| 0x0 | Libraries                 | All code needed for 3rd party code setup.            |
| 0x1 | Global resources          | All code and variables that are accessed globally.   |
| 0x2 | Classes                   | Most classes will be defined here.                   |
| 0x3 | Code Content              | The actual logic of this code file.                  |
------------------------------------------------------------------------------------------
"""
# 0x0: -----------------------------------------Libraries----------------------------------------------
# Needed for quick searching of files and directories.
import glob
# Needed for system operations.
import os
# Does simple lexical analysis.
import shlex
# Needed for launching sub-processes.
import subprocess
# Needed for doing system operations.
import sys
# Needed for time operations such as time stamps.
import time
# Needed for zipping and unzipping files.
import zipfile
# Needed for zipping and unzipping files.
import zipfile
# 0x1: -------------------------------------Global Resources-------------------------------------------


def run_compression() -> None:
    """Run the compression process.
    :return : Void | Void.
    """
    if len(sys.argv) != 3:
        print('The program got ' + str(len(sys.argv) - 1) + ' arguments but wants 2. The zip file input path and the output file path.')
        exit(0)
    input_zip_file_path = sys.argv[1]
    output_directory    = sys.argv[2]
    if output_directory[-1] != '/':
        output_directory += '/'
    c = Compressor(input_zip_file_path, output_directory)
    c.run_compression()


def get_file_type(file_path: str) -> None:
    """Return the file type from the file path provided.
    :param file_path : {str} | The path of the file to get the file type from.
    :return : Void |
    """
    cmd       = shlex.split('file --mime-type {0}'.format(file_path))
    result    = subprocess.check_output(cmd)
    mime_type = result.split()[-1]
    return mime_type.decode('ascii')

# 0x2: ---------------------------------------Core Classes---------------------------------------------


class Compressor:
    """This class will take in a zipped file, compress all PNG and JPG files inside, then re-zip.
    """

    def __init__(self, input_zip_file_path: str, output_directory: str) -> None:
        """
        :param input_zip_file_path : {str} | The path to the input zip file.
        :param output_directory    : {str} | The directory to place this program's output files.
        :temp_output_path          : {str} | The temporary location of the freshly unzipped files.
        """
        self.input_zip_file_path = input_zip_file_path
        self.output_directory    = output_directory
        self.temp_output_path    = self.output_directory + 'temp_unzipped'
        self.temp_output_path = self.output_directory + 'temp_unzipped'

    def make_zip_file(self) -> None:
        """This function will generate a zipfile out from a directory.
        :return : Void | Void.
        """
        relroot = os.path.abspath(os.path.join(self.temp_output_path, os.pardir))
        with zipfile.ZipFile(self.output_directory + 'reduced_size.zip', 'w', zipfile.ZIP_DEFLATED) as zip:
            for root, dirs, files in os.walk(self.temp_output_path):
                zip.write(root, os.path.relpath(root, relroot))
                for file in files:
                    filename = os.path.join(root, file)
                    if os.path.isfile(filename): # regular files only
                        arcname = os.path.join(os.path.relpath(root, relroot), file)
                        zip.write(filename, arcname)

    def run_compression(self) -> None:
        """Run the compression process.
        :return : Void | Void.
        """
        print('Unzipping the input file...', end='')
        start_time = time.time()
        with zipfile.ZipFile(self.input_zip_file_path, 'r') as zip_reference:
            zip_reference.extractall(self.temp_output_path)
        all_png_files           = [file for file in glob.glob(self.temp_output_path + '/**/*48.png', recursive=True)] + [file for file in glob.glob(self.temp_output_path + '/**/*100.png', recursive=True)] + [file for file in glob.glob(self.temp_output_path + '/**/*319.png', recursive=True)] + [file for file in glob.glob(self.temp_output_path + '/**/*1280.png', recursive=True)]
        all_jpeg_files          = [file for file in glob.glob(self.temp_output_path + '/**/*1280.jpeg', recursive=True)]
        all_original_files = [file for file in glob.glob(self.temp_output_path + '/**/*original', recursive=True)]
        for i, png in enumerate(all_png_files):
            print('Parsing png file : [' + str(i) + '/' + str(len(all_png_files)) + ']\t' + str(png))
            subprocess.run(['sudo', 'optipng', '-quiet', png])
        for i, jpg in enumerate(all_jpeg_files):
            print('Parsing jpg file : [' + str(i) + '/' + str(len(all_jpeg_files)) + ']\t' + str(jpg))
            subprocess.run(['sudo', 'jpegoptim', '-q', '--max=80', jpg])
        for i, original_file in enumerate(all_original_files):
            print('Parsing jpg file : [' + str(i) + '/' + str(len(all_original_files)) + ']\t' + str(jpg))
            print(get_file_type(jpg))
            subprocess.run(['sudo', 'jpegoptim', '-q', '--max=80', jpg])
        self.make_zip_file()
        print('...done!')
        print('Compression ran for ' + str('{0:2f}'.format(time.time() - start_time) + ' seconds.'))

    def __str__(self):
        return '\n' + 'self.input_zip_file_path | ' + str(self.input_zip_file_path) + '\n' + 'self.output_directory | ' + str(self.output_directory) + '\n' + 'self.temp_output_path | ' + str(self.temp_output_path) + '\n'# 0x3: ---------------------------------------Code Content---------------------------------------------
# 0xE: ----------------------------------------End of File---------------------------------------------



'''