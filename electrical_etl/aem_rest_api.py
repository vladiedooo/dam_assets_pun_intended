# coding=utf-8

"""
This module, business_checks.py, will contain business rules to check against the assets before they get uploaded.
"""

"""This module, aem_rest_api.py, contains various functionalities and utilities for working with AEM.
"""

# Needed to run threads.
import threading
# Needed for time based operations.
import time
# Used for making Basic Authorizations in HTTP.
from requests.auth import HTTPBasicAuth
# Used for making simplified HTTP calls.
import requests
# Needed for encoding into base64.
import base64

# These are the POST parameters that the CSV upload tool takes.
CHARSET                       = 'charset'
DELIMITER                     = 'delimiter'
FILE                          = 'FILE'
MULTI_DELIMITER               = 'multiDelimiter'
SEPARATOR                     = 'separator'
FILE_LOCATION                 = 'fileLocation'
IMPORT_STRATEGY               = 'importStrategy'
UPDATE_BINARY                 = 'updateBinary'
MIME_TYPE_PROPERTY            = 'mimeTypeProperty'
SKIP_PROPERTY                 = 'skipProperty'
ABSOLUTE_TARGET_PATH_PROPERTY = 'absTargetPathProperty'
RELATIVE_SOURCE_PATH          = 'relSrcPathProperty'
UNIQUE_PROPERTY               = 'uniqueProperty'
IGNORE_PROPERTIES             = 'ignoreProperties'
THROTTLE                      = 'throttle'
BATCH_SIZE                    = 'batch_size'


class AEMCalls:
	"""An API to access back-end API calls for AEM.
	"""

	def __init__(self):
		self.URL_BASE             = 'http://localhost:4502/'
		self.CSV_ASSET_IMPORT_URL = self.URL_BASE + 'acs-tools/components/csv-asset-importer'
		self.session              = None
		self.auth                 = HTTPBasicAuth('admin', 'admin')
		self.logged_in            = False

	def get_response(self, url, data=None):
		"""Get a response from AEM.
		:param url: The URL to hit.
		:param data: The data to send with, if any.
		:return: The response.
		"""
		if data is None:
			return requests.get(url, auth=self.auth)
		else:
			return requests.post(url, data=data, auth=self.auth)

	def launch_csv_asset_upload(self):
		response = self.get_response(self.CSV_ASSET_IMPORT_URL, data={SEPARATOR : ',',
		                                                              FILE_LOCATION : '/aem/crx-quickstart/bin/DAM/electrical/DAM'})
		print(response.status_code)
		print(response.content.decode('UTF-8'))


#aem = AEMCalls()
#aem.post_to_aem()

'''
	def _hidden_function(self):
		"""Utility function to run the live logger.
		:return: Void.
		"""
		while True:
			lines_printed = False
			lines         = self._get_logging_info(20)
			if lines is not None:
				for l in lines:
					if l not in self.debug_lines:
						print(l)
						self.debug_lines.add(l)
						lines_printed = True
			if lines_printed:
				time.sleep(1)
			else:
				time.sleep(5)

	self.live_logger_running = False
	self.debug_lines         = set()
	self.LOGGING_URL         = self.URL_BASE + 'system/console/slinglog/tailer.txt?tail=__NUMBER_OF_LATEST_LINES_TO_GRAB__&name=%2Flogs%2Fmy_custom_log_file.log'

	def _run_live_logger_in_background(self):
		"""This will launch a thread to continuously monitor for + print any new debug output.
		:return: Void.
		"""
		thread = threading.Thread(target=self._hidden_function, args=())
		thread.daemon = False
		thread.start()

	def _get_logging_info(self, number_of_latest_lines_to_grab):
		"""This method will get the latest x lines of debug text.
		:param number_of_latest_lines_to_grab: The number of most recent lines to grab.
		:return: The x most recent lines from the debug text.
		"""
		response      = self.get_response(self.LOGGING_URL.replace('__NUMBER_OF_LATEST_LINES_TO_GRAB__', str(number_of_latest_lines_to_grab)))
		lines_we_want = None
		if response.status_code == 200:
			content       = response.content.decode('UTF-8')
			lines         = content.split('\n')
			lines_we_want = []
			for l in lines:
				if '*DEBUG*' in l and '/bin/sku_search' in l:
					# Get rid of text we don't want from the debug message.
					da_line        = l.replace('com.scc.commerce.magento.servlets.CustomCSVAssetImporterServlet ', '')
					da_line        = da_line.replace('/bin/sku_search ', '')
					start_prune_at = da_line.rfind('*') + 1
					if 'GET' in da_line:
						end_prune_at = da_line.rfind('] GET') + 1
					else:
						end_prune_at = da_line.rfind('] POST') + 1
					da_line = da_line[0 : start_prune_at] + da_line[end_prune_at :]
					lines_we_want.append(da_line)
		return lines_we_want
'''


'''

source : https://gist.githubusercontent.com/sergeimuller/2916697/raw/5be1d62751531d940a9ef6ffb05b340dbc28ce34/gistfile1.txt

Note 1: The following CQ curl commands assumes a admin:admin username and password.
Note 2: For Windows/Powershell users: use two "" when doing a -F cURL command.
        Example: -F"":operation=delete""
Note 3: Quotes around name of package (or name of zip file, or jar) should be included.

Uninstall a bundle (use http://localhost:4505/system/console/bundles to access the Apache Felix web console)
        curl -u admin:admin -daction=uninstall http://localhost:4505/system/console/bundles/"name of bundle"

Install a bundle
        curl -u admin:admin -F action=install -F bundlestartlevel=20 -F
        bundlefile=@"name of jar.jar" http://localhost:4505/system/console/bundles

Build a bundle
        curl -u admin:admin -F bundleHome=/apps/centrica/bundles/name of bundle -F
        descriptor=/apps/centrica/bundles/com.centrica.cq.wcm.core-bundle/name_of_bundle.bnd
        http://localhost:4505/libs/crxde/build

Stop a bundle
        curl -u admin:admin http://localhost:4505/system/console/bundles/org.apache.sling.scripting.jsp
        -F action=stop

Start a bundle
        curl -u admin:admin http://localhost:4505/system/console/bundles/org.apache.sling.scripting.jsp
        -F action=start

Delete a node (hierarchy) - (this will delete any directory / node / site)
        curl -X DELETE http://localhost:4505/path/to/node/jcr:content/nodeName -u admin:admin

Upload a package AND install
        curl -u admin:admin -F file=@"name of zip file" -F name="name of package"
        -F force=true -F install=true http://localhost:4505/crx/packmgr/service.jsp

Upload a package DO NOT install
        curl -u admin:admin -F file=@"name of zip file" -F name="name of package"
        -F force=true -F install=false http://localhost:4505/crx/packmgr/service.jsp

Rebuild an existing package in CQ
        curl -u admin:admin -X POST http://localhost:4505:/crx/packmgr/service/.json/etc/packages/name_of_package.zip?cmd=build

Download (the package)
        curl -u admin:admin http://localhost:4505/etc/packages/export/name_of_package.zip > name of local package file

Upload a new package
        curl -u admin:admin -F package=@"name_of_package.zip" http://localhost:4505/crx/packmgr/service/.json/?cmd=upload

Install an existing package
        curl -u admin:admin -X POST http://localhost:4505/crx/packmgr/service/.json/etc/packages/export/name of package?cmd=install

Activate
        curl -u admin:admin -X POST -F path="/content/path/to/page" -F cmd="activate" http://localhost:4502/bin/replicate.json

Deactivate
        curl -u admin:admin -X POST -F path="/content/path/to/page" -F cmd="deactivate" http://localhost:4502/bin/replicate.json

Tree Activation
        curl -u admin:admin -F cmd=activate -F ignoredeactivated=true -F onlymodified=true
        -F path=/content/geometrixx http://localhost:4502/etc/replication/treeactivation.html

Lock page
        curl -u admin:admin -X POST -F cmd="lockPage" -F path="/content/path/to/page" -F "_charset_"="utf-8" http://localhost:4502/bin/wcmcommand

Unlock page
        curl -u admin:admin -X POST -F cmd="unlockPage" -F path="/content/path/to/page" -F "_charset_"="utf-8" http://localhost:4502/bin/wcmcommand

Copy page
        curl -u admin:admin -F cmd=copyPage -F destParentPath=/path/to/destination/parent -F srcPath=/path/to/source/locaiton http://localhost:4502/bin/wcmcommand

Further Resources:

http://cq-ops.tumblr.com/post/19017053665/useful-curl-commands
http://balawcm.wordpress.com/2013/02/13/curl-it-out-adobe-cq5-curl-commands-and-usage/

'''


