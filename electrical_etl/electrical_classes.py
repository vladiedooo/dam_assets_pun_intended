# coding=utf-8

"""This module, sku_product.py, defines the properties of an Electrical product."""


# Does simple lexical analysis.
import shlex
# Needed for launching sub-processes.
import subprocess


class ElectricalAsset:
	"""Each object instance maps one to one with an asset file."""

	MIME_TYPE_TO_FILE_ENDING = {'image/jpeg': 'jpg', 'image/tiff': 'tif'}

	def __init__(self, file_path):
		self.file_path = file_path
		self.file_name = self.file_path[self.file_path.rfind('/') + 1:]
		if '.' in self.file_name:
			self.file_ending = self.file_name[self.file_name.rfind('.') + 1:]
		else:
			self.file_ending = None
		self.relative_directory_path = self.file_path[self.file_path.rfind('electrical_files/DAM') + len('electrical_files/DAM') + 1:].replace(self.file_name, '').replace(' ', '_')
		self.mime_type  = self._get_mime_type()
		self.clean      = True
		self.failed_at  = None
		self.final_name = None

	def get_name(self):
		"""Utility function.
		:return: The latest name."""
		if self.final_name is not None:
			return self.final_name
		return self.file_name

	def fix_name(self):
		"""Renames the file to have a file extension that matches its mime type.
		:return: Void."""
		self.file_ending = ElectricalAsset.MIME_TYPE_TO_FILE_ENDING[self.mime_type]
		self.final_name  = self.file_name + '.' + self.file_ending
		# TODO : Actually fix the file here...

	def error_check_failed_at_(self, error_check_failed):
		"""Sets this asset to be non-clean.
		:param error_check_failed: The error check that this SKU failed and could not be fixed for.
		:return: Void."""
		self.failed_at = error_check_failed
		self.clean     = False

	def _get_mime_type(self):
		cmd       = shlex.split('file --mime-type {0}'.format('"' + self.file_path + '"'))
		result    = subprocess.check_output(cmd)
		mime_type = result.split()[-1]
		return mime_type.decode('ascii')

	def __str__(self):
		if self.final_name is not None:
			return self.final_name + '{' + str(self.mime_type) + '} - ' + self.relative_directory_path
		else:
			return self.file_name + '{' + str(self.mime_type) + '} - ' + self.relative_directory_path


class ElectricalProduct:
	"""Each object instance maps one to one with a unique SKU."""

	CSV_COLUMN_ORDER = ['sku', 'primary image name', 'secondary image', 'tertiary image', 'quaternary image',
	                    'quinary image', 'product name', 'original image name', 'pdf-warranty', 'pdf-instructions',
						'pdf-miscellaneous', 'pdf-miscellaneous 3', 'pdf-miscellaneous 2', 'pdf-spec sheet',
						'pdf-spec sheet 2', 'pdf-sell sheet', 'pdf-sell sheet2']

	def __init__(self, properties):
		self.properties = properties
		self.sku_clones = []
		self.clean      = True
		self.failed_at  = None

	def get_sub_properties_dictionary_for_keys_that_require_asset_file_matching(self):
		"""Returns a new properties dictionary that will have the same key and values except will only include keys that require its value to be matched as an actual asset file image.
		:return: The sub dictionary."""
		sub_properties = {}
		for k in self.properties:
			if k != 'sku' and k != 'original image name' and k != 'product name' and str(self.properties[k]).lower().strip() != 'use generic image':
				sub_properties[k] = self.properties[k]
		return sub_properties

	def error_check_failed_at_(self, error_check_failed):
		"""Sets this SKU to be non-clean.
		:param error_check_failed: The error check that this SKU failed and could not be fixed for.
		:return: Void."""
		self.failed_at = error_check_failed
		self.clean     = False

	def add_sku_clone(self, electrical_product_clone):
		"""Not an actual clone but a duplicate SKU requires both instances to be checked.
		:param electrical_product_clone: The ElectricalProduct object to keep track of for later error checks.
		:return: Void."""
		self.sku_clones.append(electrical_product_clone)

	def get_primary_image(self) -> str:
		"""Returns the primary image value.
		:return: The primary image value as a string."""
		return self.properties['primary image name']

	def get_sku(self) -> str:
		"""Return the unique SKU of this product.
		:return: The unique SKU as a string."""
		return self.properties['sku']

	def get_product_name(self) -> str:
		"""Get the name of this product.
		:return: The name of the product as a string."""
		return self.properties['product name']

	def get_properties_in_correct_csv_order(self):
		"""Returns this objects properties in csv data format, making sure to follow a pre-defined header order as well.
		:return: The CSV row as a String."""
		csv_string = ''
		for i, c in enumerate(ElectricalProduct.CSV_COLUMN_ORDER):
			element_matched = False
			matched_element = None
			for k in self.properties:
				if k == c:
					element_matched = True
					matched_element = k
					print('The matched element is : ' + str(matched_element))
			if element_matched:
				csv_string += matched_element + ','
			else:
				csv_string += ','

	def __str__(self):
		return 'SKU{' + self.get_sku() + '} : ' + str(self.properties)
