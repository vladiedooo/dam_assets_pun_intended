# -*- coding: utf-8 -*-

"""
This module, anything_time_related, just contains utility functions and classes for anything time related.

last generated : 2017-03-13 12:56:29

Base File Layout:
------------------------------------------------------------------------------------------
| 0x0 | Libraries                 | All code needed for 3rd party code setup.            |
| 0x1 | Global resources          | All code and variables that are accessed globally.   |
| 0x2 | Classes                   | Most classes will be defined here.                   |
| 0x3 | Code Content              | The actual logic of this code file.                  |
------------------------------------------------------------------------------------------
"""
# 0x0: -----------------------------------------Libraries----------------------------------------------
# Needed for doing operations relating to date and time.
import calendar
# Needed for doing operations relating to date and time.
import datetime
# Needed for generating random numbers.
import random
# Needed for time operations such as time stamps.
import time
# 0x1: -------------------------------------Global Resources-------------------------------------------


def get_datetime_for_a_random_day_of_this_month_as_unix_time():
	"""This is a utility function needed for testing purposes.
	:return: str | A specific unix time.
	"""
	now = datetime.datetime.now()
	number_of_days_in_this_month = calendar.monthrange(now.year, now.month)[1]
	new_random_day = now.replace(day=random.randint(1, number_of_days_in_this_month))
	return time.mktime(new_random_day.timetuple())


# 0x2: ---------------------------------------Most Classes---------------------------------------------


class EasyTimer:
	"""A timer that starts once the object is initialized and prints elapsed time on str() method. There is no stop on this clock."""

	def __init__(self) -> None:
		"""The constructor.
		start_time | The start time of this clock. It will be during object creation.
		"""
		self._start_time          = time.time()
		self._manually_stopped    = None
		self._run_time            = None
		self._last_time_increment = None

	def _get_formatted_time(self):
		return str('{0:2f}'.format(time.time() - self._start_time)) + 's'

	def _get_formatted_time_increment(self):
		temp = self._last_time_increment
		self._last_time_increment = time.time()
		return str('{0:2f}'.format(time.time() - temp)) + 's'

	def stop(self):
		"""This stops the timer. If this function is not called the timer will automatically stop on output printing.
		:return:"""
		self._run_time         = self._get_formatted_time()
		self._manually_stopped = 'elapsed time : ' + self._run_time

	def get_run_time(self):
		"""This returns the timers run duration. If the stop function has not yet been called then this function will call it first.
		:return: The total run time of this clock as a float."""
		if self._manually_stopped is None:
			self.stop()
		return self._run_time

	def get_incremental_time(self):
		"""This returns the current total duration plus the time delta from the last call to 'get_incremental_time'.
		:return: A string."""
		if self._last_time_increment is None:
			self._last_time_increment = time.time()
			return 'elapsed time : ' + self._get_formatted_time()
		else:
			return 'elapsed time : ' + self._get_formatted_time() + ', delta time : ' + self._get_formatted_time_increment()

	def __str__(self) -> str:
		if self._manually_stopped is None:
			return 'elapsed time : ' + str('{0:2f}'.format(time.time() - self._start_time)) + 's'
		return self._manually_stopped


class IterativeStopClock:
	"""Currently only a single use class so it will not be heavily documented (yet)."""

	def __init__(self, iteration_sleep_time : int, total_wait_time : int) -> None:
		"""The constructor.
		elapsed_wait_time    | How long the clock has waited so far for.
		iteration_sleep_time | The duration of time to sleep for each iteration.
		keep_running         | A boolean indicating if this clock finished naturally or not.
		total_wait_time      | How long this clock needs to wait for.
		"""
		self._elapsed_wait_time    = 0
		self._iteration_sleep_time = iteration_sleep_time
		self._keep_running         = True
		self._total_wait_time      = total_wait_time

	def completed_naturally(self) -> bool:
		"""This function returns a boolean indicating if the clock finished on its own {1} or was stopped {0}.
		:return: bool | {0} - was stopped. {1} - finished on its own.
		"""
		return self._keep_running

	def should_keep_iterating(self) -> bool:
		"""This function returns a boolean indicating if the IterativeStopClock instance should keep on iterating {1} or not {0}.
		:return: bool | {0} - should not keep iterating, {1] - should keep iterating.
		"""
		return(self._elapsed_wait_time < self._total_wait_time) and self._keep_running

	def sleep_an_iteration(self) -> None:
		"""This function call will have the current thread sleep for the iteration sleep time duration.
		:return: None | Void.
		"""
		time.sleep(self._iteration_sleep_time)
		self._elapsed_wait_time += self._iteration_sleep_time

	def stop(self) -> None:
		"""This function stops the IterativeStopClock instance and the method 'completed_naturally' will now return 'False'.
		:return: None | Void.
		"""
		self._keep_running = False

# 0x3: ---------------------------------------Code Content---------------------------------------------
# 0xE: ----------------------------------------End of File---------------------------------------------
