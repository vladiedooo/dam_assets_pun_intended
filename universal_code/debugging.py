#!/usr/bin/env python3
# coding=utf-8

"""
This module, debugging.py, will contain code related to debugging (such as printing error messages).
"""

#import sys
#sys.path.insert(0, '/home/dev_usr/urbtek')
#from universal_code import system_operations as so


class MyException(Exception):
	"""
	Just something useful to have to throw some of my own custom exception.
	"""
	pass


class ParameterException(Exception):
	"""
	A custom exception for when a function receives bad parameter data.
	"""
	def __init__(self, message):
		super(ParameterException, self).__init__(message)


class AbstractMethodNotImplementedException(Exception):
	"""
	A custom exception for when a function gets called that hasn't been set in a child class.
	"""
	def __init(self, message):
		super(AbstractMethodNotImplementedException, self).__init__(message)


def raise_exception(exception, message):
	raise exception(message)

TCP_LOCAL_HOST = 'tcp://127.0.0.1:'
LOCAL_HOST = '127.0.0.1'
NEXUS_DEV_RECEIVE_PORT                 = 40000
NEXUS_DEV_MANUAL_COMMUNICATION_PORT    = 40001
NEXUS_DEV_AUTOMATED_COMMUNICATION_PORT = 40002
starting_port = NEXUS_DEV_AUTOMATED_COMMUNICATION_PORT + 1
def get_a_free_port():
	global starting_port
	# We can assume ports are free because ports above 30000 have been sealed off.
	# TODO: THIS WILL BREAK WHEN MORE THAN DEV EXISTS.
	starting_port += 1
	return starting_port - 1

# Terminal font coloring and styling.

class TextColors:
	HEADER    = '\033[95m'
	OK_BLUE   = '\033[94m'
	OK_GREEN  = '\033[92m'
	WARNING   = '\033[93m'
	FAIL      = '\033[91m'
	ENDC      = '\033[0m'
	BOLD      = '\033[1m'
	UNDERLINE = '\033[4m'

def print_text_with_color(text, color, end=None):
	if end is None:
		print(color + text + TextColors.ENDC + '\n')
	else:
		print(color + text + TextColors.ENDC, end='')

def terminate(termination_message=''):
	if termination_message is '':
		print_text_with_color('Program termination has been initiated, good bye!', TextColors.FAIL)
	else:
		print_text_with_color(termination_message, TextColors.WARNING, '')
		if not termination_message.endswith('.'):
			print_text_with_color('. The program will now terminate.', TextColors.FAIL)
		else:
			print_text_with_color(' The program will now terminate.', TextColors.FAIL)
	exit()
