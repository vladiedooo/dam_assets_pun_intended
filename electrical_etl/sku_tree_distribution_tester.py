# coding=utf-8

"""This module, sku_tree_distribution_tester.py, tests various configurations of the generated node tree based off of SKUs."""


# Needed for getting the standard deviation.
import statistics


class TreeTester:
	"""This object will test the various levels that the SKU nodes can be arranged."""

	def __init__(self, products):
		self.sku_list = []
		for p in products:
			self.sku_list.append(p.get_sku())

	def _test_level_(self, level):
		sku_dictionary = {}
		for sku in self.sku_list:
			if sku[:level] not in sku_dictionary:
				sku_dictionary[sku[:level]] = ['node']
			else:
				sku_dictionary[sku[:level]].append('node')

		number_of_child_nodes = 0
		nums                  = []
		for sku in sku_dictionary:
			number_of_child_nodes += len(sku_dictionary[sku])
			nums.append(len(sku_dictionary[sku]))
		average_number_of_child_nodes = float(number_of_child_nodes) / float(len(sku_dictionary))

		return len(sku_dictionary), average_number_of_child_nodes, statistics.stdev(nums)

	def print_tree_configurations(self):
		"""Run analysis to find the preferred tree configuration.
		:return: Void."""
		longest_sku  = -1
		smallest_sku = 99999
		for sku in self.sku_list:
			sku_length = len(sku)
			if sku_length > longest_sku:
				longest_sku = sku_length
			if sku_length < smallest_sku:
				smallest_sku = sku_length
		index = 1
		while index != smallest_sku + 1:
			a, b, c = self._test_level_(index)
			print('# of characters of {' + str(index) + '} gives : ' + str(a) + ' folders with an average of ' + str(b) + ' nodes per folder. With a standard deviation of ' + str(c) + '. For a total of around ' + str(int(float(a) * float(b))) + ' total files + folders.')
			index += 1