# coding=utf-8

"""This module, electrical_error_checks.py, will run all the error checks for the Electrical ETL Process."""

# Used to represent electrical asset files.
from electrical_etl.electrical_classes import ElectricalAsset
# Used to represent unique SKUs as products.
from electrical_etl.electrical_classes import ElectricalProduct
# Used to help the IDE.
from typing import List
# Needed for doing valid Image check operations.
from PIL import Image


class ElectricalAutomaticSolutions:
	"""Fixes simple errors that are easy to fix."""

	_FIX_KEY_FOR_ERROR_0 = 'Fixes SKU to be AEM compliant.'
	_FIX_KEY_FOR_ERROR_4 = 'Fixes asset names that have missing file extensions.'
	_FIX_KEY_FOR_ERROR_5 = 'Fixed a mist-matched file extension to match its contents mime-type.'

	MIME_TYPE_TO_FILE_ENDING = {'image/jpeg': 'jpg'}

	def __init__(self):
		self.solutions_made = {ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_0: [],
		                       ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_4: [],
		                       ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_5: []}

	def get_number_of_product_solutions(self):
		"""Returns the number of Products fixed.
		:return: Void."""
		return len(self.solutions_made[ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_0])

	def get_number_of_asset_solutions(self):
		"""Returns the number of Assets fixed.
		:return: Void."""
		return len(self.solutions_made[ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_4]) + len(self.solutions_made[ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_5])

	def fix_error_0(self, product: ElectricalProduct):
		"""Adds a solution to error 0.
		:param product : The product to fix.
		:return: Void."""
		bad_sku = product.properties['sku']
		product.properties['sku'] = product.properties['sku'].replace('/', '_')
		self.solutions_made[ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_0].append('Fixed the SKU {' + bad_sku + '} to {' + product.get_sku() + '}')

	def fix_error_4(self, asset: ElectricalAsset):
		"""Adds a solution to error 4.
		:param asset : The asset to fix.
		:return: Void."""
		asset.fix_name()
		self.solutions_made[ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_4].append('Fixed the asset from {' + asset.file_name + '} to {' + asset.final_name + '} based off its mime-type of {' + asset.mime_type + '}')

	def fix_error_5(self, asset: ElectricalAsset):
		"""Adds a solution to error 5.
		:param asset: The asset to fix.
		:return: Void."""
		asset.fix_name()
		self.solutions_made[ElectricalAutomaticSolutions._FIX_KEY_FOR_ERROR_5].append('Fixed the asset from {' + asset.file_name + '} to {' + asset.final_name + '} based off its mime-type of {' + asset.mime_type + '}')


class ElectricalErrorChecker:
	"""Runs error checks on the Electrical ETL process."""

	_ERROR_TEXT_1 = 'Duplicate SKU row with mis-matched values, usually the product name.'
	_ERROR_TEXT_2 = 'No value found for the primary image.'
	_ERROR_TEXT_3 = 'Faulty primary image value.'
	_ERROR_TEXT_4 = 'Invalid asset file content type.'
	_ERROR_TEXT_6 = 'Check if file is corrupt.'
	_ERROR_TEXT_7 = 'Asset not found!'

	# For error check 3.
	DISCONTINUED                = 'discontinued?'
	SHOULD_THESE_BE_LISTED      = 'should these be listed?'
	USE_GENERIC_IMAGE           = 'use generic image'
	FAULTY_PRIMARY_IMAGE_VALUES = [DISCONTINUED, SHOULD_THESE_BE_LISTED]

	# For error check 4.
	VALID_FILE_ENDING_TO_MIME_TYPES   = {'pdf': 'application/pdf', 'jpg': 'image/jpeg', 'tif': 'image/tiff'}
	INVALID_FILE_ENDING_TO_MIME_TYPES = {'doc': 'application/msword', 'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	                                     '@@@@@@@@@@@@@': 'inode/x-empty', '@@@@@@@@@@@@': 'image/vnd.adobe.photoshop', '@@@@@@@@@@@@@@@': 'application/octet-stream'}

	def __init__(self, products: List[ElectricalProduct], assets: List[ElectricalAsset]):
		self.products     = products
		self.assets       = assets
		self.solutions    = ElectricalAutomaticSolutions()
		self.errors       = {ElectricalErrorChecker._ERROR_TEXT_1: [],
		                     ElectricalErrorChecker._ERROR_TEXT_2: [],
		                     ElectricalErrorChecker._ERROR_TEXT_3: [],
		                     ElectricalErrorChecker._ERROR_TEXT_4: [],
		                     ElectricalErrorChecker._ERROR_TEXT_6: [],
		                     ElectricalErrorChecker._ERROR_TEXT_7: []}

	def print_solutions(self):
		"""Prints any found solutions.
		:return: Void."""
		for s in self.solutions.solutions_made:
			print(s)
			for e in self.solutions.solutions_made[s]:
				print(e)
			print('----------------------------')

	def run_error_checks_and_polish_product_objects_then_return_information_output(self):
		"""Runs the error checks and updates the product objects.
		:return: Void."""
		for p in self.products:
			self._error_check_0(p)
			if len(p.sku_clones) > 0:
				self._error_check_1(p)
			self._error_check_2(p)

			if p.clean:
				self._error_check_3(p)

		for a in self.assets:
			self._error_check_4(a)

		for a in self.assets:
			if a.clean:
				self._error_check_5(a)
				self._error_check_6(a)

		# Now that products and assets are cleaned separately, now they are linked together and checked for other errors.
		self._error_check_7()

		number_of_clean_products = 0
		number_of_dity_products  = 0
		for p in self.products:
			if p.clean:
				number_of_clean_products += 1
			else:
				number_of_dity_products  += 1

		return number_of_clean_products, number_of_dity_products, self.solutions.get_number_of_product_solutions(), self.solutions.get_number_of_asset_solutions()

	def _error_check_0(self, product: ElectricalProduct):
		for c in product.get_sku():
			if not c.isdigit() and not c.isalpha() and c != '_' and c != '-':
				self.solutions.fix_error_0(product)
				break

	def _add_error(self, product, error_type, error_message):
		self.errors[error_type].append('The SKU {' + product.get_sku() + '} ' + error_message)
		product.error_check_failed_at_(error_type)

	def _add_error_for_asset(self, asset, error_type, error_message):
		self.errors[error_type].append('The asset {' + asset.file_name + '} ' + error_message)
		asset.error_check_failed_at_(error_type)

	def _error_check_1(self, product: ElectricalProduct):
		self._add_error(product, ElectricalErrorChecker._ERROR_TEXT_1, 'has a duplicate row with mis-matched values, usually the product name.')

	def _error_check_2(self, product: ElectricalProduct):
		if 'primary image name' not in product.properties:
			self._add_error(product, ElectricalErrorChecker._ERROR_TEXT_2, 'does not have a value for the primary image column.')

	def _error_check_3(self, product: ElectricalProduct):
		if product.get_primary_image().lower().strip() in ElectricalErrorChecker.FAULTY_PRIMARY_IMAGE_VALUES or len(product.get_primary_image().strip()) == 0:
			self._add_error(product, ElectricalErrorChecker._ERROR_TEXT_3, 'has a bad primary image value of {' + str(product.get_primary_image()) + '}.')

	def _error_check_4(self, asset: ElectricalAsset):
		if asset.file_ending not in ElectricalErrorChecker.VALID_FILE_ENDING_TO_MIME_TYPES or asset.mime_type not in ElectricalErrorChecker.VALID_FILE_ENDING_TO_MIME_TYPES.values():
			if asset.mime_type in ElectricalErrorChecker.INVALID_FILE_ENDING_TO_MIME_TYPES.values():
				self._add_error_for_asset(asset, ElectricalErrorChecker._ERROR_TEXT_4, 'has the invalid mime type of {' + asset.mime_type + '}')
			else:
				if asset.file_ending is None:
					self.solutions.fix_error_4(asset)
				else:
					self._add_error_for_asset(asset, ElectricalErrorChecker._ERROR_TEXT_4, 'has something invalid, here is the mime type {' + str(asset.mime_type) + '}.')

	def _error_check_5(self, asset: ElectricalAsset):
		if ElectricalErrorChecker.VALID_FILE_ENDING_TO_MIME_TYPES[asset.file_ending] != asset.mime_type:
			self.solutions.fix_error_5(asset)

	def _error_check_6(self, asset: ElectricalAsset):
		if asset.file_ending == 'jpg':
			try:
				image = Image.open(asset.file_path)
				image.verify()
			except Exception as e:
				self._add_error_for_asset(asset, ElectricalErrorChecker._ERROR_TEXT_6, 'could not be parsed. Is this file corrupt? {' + str(asset.file_name) + '}, error message is : {' + str(e) + '}')

	def _error_check_7(self):
		for p in self.products:
			for k in p.properties:
				if k != 'sku' and k != 'original image name' and k != 'product name' and str(p.properties[k]).lower().strip() != 'use generic image':
					search_value = str(p.properties[k])
					found        = False
					for a in self.assets:
						if search_value == a.file_name:
							found = True
						if a.final_name is not None:
							if search_value == a.final_name:
								found = True
					if not found:
						if p.clean:
							self._add_error(p, ElectricalErrorChecker._ERROR_TEXT_7, 'the file {' + search_value + '} was not found in the DAM.')

	# TODO : Check values for having valid characters :X

