# coding=utf-8

"""This module, excel_parser.py, will read in data from Excel files."""

# Used to get absolute path.
import os
# Used to read Excel files.
import openpyxl
# Used to represent unique SKUs as products.
from electrical_etl.electrical_classes import ElectricalProduct
# Used to represent unique Asset files from the DAM.
from electrical_etl.electrical_classes import ElectricalAsset
# Used to help the IDE.
from typing import List
# Used for creating CSV files.
import csv


# TODO : Clean up this file.


class ElectricalExcelParser:
	"""This objects reads in the data from an Excel file."""

	def __init__(self, file_path):
		self.file_path   = file_path
		self.workbook    = None
		self.worksheets  = []
		self.headers     = {'sku': 0,
		                    'primary image name': 1,
		                    'secondary image': 2,
		                    'tertiary image': 3,
		                    'quaternary image': 4,
		                    'quinary image': 5,
		                    'product name': 6,
		                    'original image name': 7,
		                    'pdf-warranty': 8,
		                    'pdf-instructions': 9,
		                    'pdf-miscellaneous': 10,
		                    'pdf-miscellaneous 3': 11,
		                    'pdf-miscellaneous 2': 12,
		                    'pdf-spec sheet': 13,
		                    'pdf-spec sheet 2': 14,
		                    'pdf-sell sheet': 15,
		                    'pdf-sell sheet2': 16}
		self.skus        = {}

	def parse_out_products(self, sheet_names_to_ignore=None):
		"""Read the file into program memory.
		:param sheet_names_to_ignore: Any sheet names to not parse.
		:return: Void."""
		if sheet_names_to_ignore is None:
			sheet_names_to_ignore = []
		self.workbook = openpyxl.load_workbook(self.file_path, read_only=True)
		sheet_names   = self.workbook.get_sheet_names()
		for sn in sheet_names:
			if sn.lower().strip() not in sheet_names_to_ignore:
				self.worksheets.append(WorksheetGeneric(sn, self.workbook[sn]))
		self.workbook.close()

		# Go through each worksheet.
		for work_sheet in self.worksheets:
			# Go through each row.
			for row in work_sheet.rows:
				# Go through each element and generate the properties dictionary.
				properties = {}
				for i, element in enumerate(row):
					if element.strip() != 'None':
						column_name = str(work_sheet.headers[i])
						if i == 0:
							sku = str(element).strip()
							properties[column_name] = sku
						else:
							properties[column_name] = str(element)
				if sku not in self.skus:
					self.skus[sku] = ElectricalProduct(properties)
				else:
					if len(properties) > 0:
						if properties != self.skus[sku].properties:
							self.skus[sku].add_sku_clone(ElectricalProduct(properties))

		return self.skus.values()


class WorksheetGeneric:
	"""This objects represents a single worksheet in an Excel file."""

	def __init__(self, sheet_name, worksheet):
		self.sheet_name = sheet_name
		self.worksheet  = worksheet
		self.headers    = []
		self.rows       = []
		for i, row in enumerate(self.worksheet.rows):
			if i == 0:
				for cell in row:
					self.headers.append(str(cell.value).strip().lower())
			else:
				single_row = []
				for cell in row:
					single_row.append(str(cell.value).strip())
				self.rows.append(single_row)


class AEMCSVAssets:
	"""This object will construct the CSV data from the products and assets."""

	def __init__(self, products: List[ElectricalProduct], assets: List[ElectricalAsset], path_to_final_csv):
		self.products          = products
		self.assets            = assets
		self.path_to_final_csv = path_to_final_csv
		self.final_csv         = ''
		self.CSV_COLUMN_ORDER  = ['sku', 'primary image name', 'secondary image', 'tertiary image', 'quaternary image',
		                    'quinary image', 'product name', 'original image name', 'pdf-warranty', 'pdf-instructions',
		                    'pdf-miscellaneous', 'pdf-miscellaneous 3', 'pdf-miscellaneous 2', 'pdf-spec sheet',
		                    'pdf-spec sheet 2', 'pdf-sell sheet', 'pdf-sell sheet2']

		self.PRODUCT_REFERENCE_START = '/etc/commerce/products/electrical/en/buckets/'

		# 0x00 | ../metadata/SKU {{ String }}                 | SKU
		# 0x01 | cq:productReference {{ String }}             | example : /etc/commerce/products/sktools/sktools/en/buckets/40/40971
		# 0x02 | dam:productID {{ String }}                   | SKU
		# 0x03 | dc:title {{ String : multi }}                | product name
		# 0x04 | relSrcPath                                   | relative source path
		# 0x05 | imageName {{ String }}                       | primary image name
		# 0x06 | absTargetPath                                | DAM path, example : /content/dam/sktools/ecommerce/drive-tools/skuXYZ-product-name.png
		# 0x07 | ../metadata/primaryImage {{ String }}        | an image name (TODO : should it be some kind of SKU node?)
		# 0x08 | ../metadata/secondaryImage {{ String }}      | an image name (TODO : should it be some kind of SKU node?)
		# 0x09 | ../metadata/tertiaryImage {{ String }}       | an image name (TODO : should it be some kind of SKU node?)
		# 0x10 | ../metadata/quaternaryImage {{ String }}     | an image name (TODO : should it be some kind of SKU node?)
		# 0x11 | ../metadata/secondaryImage {{ String }}      | an image name (TODO : should it be some kind of SKU node?)
		# 0x12 | ../metadata/quinaryImage {{ String }}        | an image name (TODO : should it be some kind of SKU node?)
		# 0x13 | ../metadata/pdf-warranty {{ String }}        | asset name (TODO : should it be something else?)
		# 0x14 | ../metadata/pdf-instructions {{ String }}    | asset name (TODO : should it be something else?)
		# 0x15 | ../metadata/pdf-spec-sheet {{ String }}      | asset name (TODO : should it be something else?)
		# 0x16 | ../metadata/pdf-spec-sheet-2 {{ String }}    | asset name (TODO : should it be something else?)
		# 0x17 | ../metadata/pdf-sell-sheet {{ String }}      | asset name (TODO : should it be something else?)
		# 0x18 | ../metadata/pdf-sell-sheet-2 {{ String }}    | asset name (TODO : should it be something else?)
		# 0x19 | ../metadata/pdf-miscellaneous {{ String }}   | asset name (TODO : should it be something else?)
		# 0x20 | ../metadata/pdf-miscellaneous-1 {{ String }} | asset name (TODO : should it be something else?)
		# 0x21 | ../metadata/pdf-miscellaneous-2 {{ String }} | asset name (TODO : should it be something else?)
		# 0x22 | ../metadata/pdf-miscellaneous-3 {{ String }} | asset name (TODO : should it be something else?)
		# 0x23 | cq:tags {{ String : multi }}                 | the name of all the folders in the relative path
		# 0x24 | ../metadata/parentCategory {{ String }}      | the parent category
		# 0x25 | ../metadata/subCategory {{ String }}         | the sub category

		self.CSV_HEADERS = [
		'../metadata/SKU {{ String }}',
		'cq:productReference {{ String }}',
		'dam:productID {{ String }}',
		'dc:title {{ String : multi }}',
		'relSrcPath',
		'imageName {{ String }}',
		'absTargetPath',
		'../metadata/primaryImage {{ String }}',
		'../metadata/secondaryImage {{ String }}',
		'../metadata/tertiaryImage {{ String }}',
		'../metadata/quaternaryImage {{ String }}',
		'../metadata/secondaryImage {{ String }}',
		'../metadata/quinaryImage {{ String }}',
		'../metadata/pdf-warranty {{ String }}',
		'../metadata/pdf-instructions {{ String }}',
		'../metadata/pdf-spec-sheet-2 {{ String }}',
		'../metadata/pdf-sell-sheet {{ String }}',
		'../metadata/pdf-sell-sheet-2 {{ String }}',
		'../metadata/pdf-miscellaneous {{ String }}',
		'../metadata/pdf-miscellaneous-2 {{ String }}',
		'../metadata/pdf-miscellaneous-3 {{ String }}',
		]
		#'cq:tags {{ String : multi }}',
		#'../metadata/parentCategory {{ String }}',
		#'../metadata/subCategory {{ String }}']

		self.COLUMN_TO_AEM_PROPERTY = {'sku'                 : ['../metadata/SKU {{ String }}', 'dam:productID {{ String }}'],
		                               'primary image name'  : ['imageName {{ String }}', '../metadata/primaryImage {{ String }}'],
		                               'secondary image'     : '',
		                               'tertiary image'      : '',
		                               'quaternary image'    : '',
		                               'quinary image'       : '',
		                               'product name'        : ['dc:title {{ String : multi }}'],
		                               'original image name' : '',
		                               'pdf-warranty'        : '',
		                               'pdf-instructions'    : '',
		                               'pdf-spec sheet'      : '',
		                               'pdf-spec sheet 2'    : '',
		                               'pdf-sell sheet'      : '',
		                               'pdf-sell sheet 2'    : ''
		                               }
		# TODO : I shouldn't have to run this logic here.
		self.actually_clean_products = []
		for p in self.products:
			sub_properties = p.get_sub_properties_dictionary_for_keys_that_require_asset_file_matching()
			found_in_dam = False
			for k in sub_properties:
				search_value = sub_properties[k]
				for a in self.assets:
					if search_value == a.get_name():
						found_in_dam = True
			if not found_in_dam:
				#print('Asset not found for : ' + str(p.get_sku()))
				y = 2
			else:
				self.actually_clean_products.append(p)

		self._generate_final_csv()

	def _get_property_or_just_empty_string(self, product, key):
		if key in product.properties:
			return product.properties[key]
		return ''

	def _generate_final_csv(self):
		# Add the headers.
		for h in self.CSV_HEADERS:
			self.final_csv += str(h) + ','
		self.final_csv = self.final_csv[0:-1] + '\n'

		# Go through each product.
		for p in self.actually_clean_products:
			sku                  = p.get_sku()
			product_reference    = self.PRODUCT_REFERENCE_START + sku[:2] + '/' + sku
			product_id           = sku
			product_title        = p.get_product_name()

			_combined_name       = str(sku + '-' + product_title).replace(' ', '_')

			# Oh, this is to be used later.
			node_path            = '/etc/commerce/products/electrical/electrical/en/buckets'

			asset_relative_path  = ''
			# ABSOLUTE FILE DUMP LOCATION : /aem/crx-quickstart/bin/DAM/electrical/DAM
			just_the_relative    = ''
			asset_file_ending    = ''
			# Find this product's asset match. TODO : make a better system.
			image_name = 'not found'
			for a in self.assets:
				if p.get_primary_image() == a.get_name():
					asset_file_ending   = a.file_ending
					image_name          = a.get_name()
					asset_relative_path += a.relative_directory_path + image_name
					just_the_relative   = a.relative_directory_path

			absolute_target_path      = '/content/DAM/electrical/DAM/' + just_the_relative + _combined_name + '.' + str(asset_file_ending)

			metadata_primary_image       = self._get_property_or_just_empty_string(p, 'primary image name')
			metadata_secondary_image     = self._get_property_or_just_empty_string(p, 'secondary image')
			metadata_tertiary_image      = self._get_property_or_just_empty_string(p, 'tertiary image')
			metadata_quaternary_image    = self._get_property_or_just_empty_string(p, 'quaternary image')
			metadata_quinary_image       = self._get_property_or_just_empty_string(p, 'quinary image')

			metadata_pdf_warranty        = self._get_property_or_just_empty_string(p, 'pdf-warranty')
			metadata_pdf_instructions    = self._get_property_or_just_empty_string(p, 'pdf-instructions')

			metadata_pdf_spec_sheet      = self._get_property_or_just_empty_string(p, 'pdf-spec sheet')
			metadata_pdf_spec_sheet_2    = self._get_property_or_just_empty_string(p, 'pdf-spec sheet 2')
			metadata_pdf_sell_sheet      = self._get_property_or_just_empty_string(p, 'pdf-sell sheet')
			metadata_pdf_sell_sheet_2    = self._get_property_or_just_empty_string(p, 'pdf-sell sheet 2')

			metadata_pdf_miscellaneous   = self._get_property_or_just_empty_string(p, 'pdf-miscellaneous')
			metadata_pdf_miscellaneous_2 = self._get_property_or_just_empty_string(p, 'pdf-miscellaneous 2')
			metadata_pdf_miscellaneous_3 = self._get_property_or_just_empty_string(p, 'pdf-miscellaneous 3')

			if image_name != 'not found':

				values = [sku, product_reference, product_id, product_title, asset_relative_path, image_name, absolute_target_path,
				          metadata_primary_image, metadata_secondary_image, metadata_tertiary_image, metadata_quaternary_image, metadata_quinary_image,
				          metadata_pdf_warranty, metadata_pdf_instructions, metadata_pdf_spec_sheet, metadata_pdf_spec_sheet_2, metadata_pdf_sell_sheet,
				          metadata_pdf_sell_sheet_2, metadata_pdf_miscellaneous, metadata_pdf_miscellaneous_2, metadata_pdf_miscellaneous_3]

				# TODO : Bad values like these should have been parsed out or fixed earlier.
				values_are_good = True
				for v in values:
					if ',' in v:
						values_are_good = False

				if values_are_good:

					print('Product reference : ' + str(product_reference))
					print('Asset relative path : ' + str(asset_relative_path))
					print('Absolute target path : ' + str(absolute_target_path))
					print('----------------------------------------------------\n')

					for v in values:
						#print(v)
						self.final_csv += str(v) + ','
					self.final_csv = self.final_csv[0:-1] + '\n'

				#print('-------------------------')

		# Now create the CSV file.
		with open(self.path_to_final_csv, 'w+', ) as file_handler:
			for line in self.final_csv.split('\n'):
				file_handler.write(line + '\n')

	def get_final_csv(self):
		return self.final_csv

	def get_sub_sample_of_final_csv(self, number_of_products_to_get):
		rows                      = self.final_csv.split('\n')
		csv                       = ''
		number_of_products_gotten = 0
		for i, r in enumerate(rows):
			if i == 0:
				csv += r + '\n'
			else:
				if number_of_products_gotten < number_of_products_to_get:
					csv += r + '\n'
					number_of_products_gotten += 1
		return csv

